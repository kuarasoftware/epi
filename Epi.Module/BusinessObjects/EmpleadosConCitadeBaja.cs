﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DomainComponent]
    [DefaultClassOptions]
    public class EmpleadosConCitadeBaja : IXafEntityObject, IObjectSpaceLink, INotifyPropertyChanged
    {
        private IObjectSpace objectSpace;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public EmpleadosConCitadeBaja()
        {
        }
        [Browsable(false)]  // Hide the entity identifier from UI.
        public int ID { get; set; }

        private Temel.Employee _Empleado;

        public Temel.Employee Empleado
        {
            get { return _Empleado; }
            set { _Empleado = value; }
        }

        private Citas _Cita;

        public Citas Cita
        {
            get { return _Cita; }
            set { _Cita = value; }
        }
        //FechaCita,[From Date],[To Date],[Cause of Absence Code]
        private DateTime _desde;
        public DateTime Desde
        {
            get { return _desde; }
            set {
                if (value.Year == 1753) value = DateTime.MinValue;
                _desde = value; }
        }
        private DateTime _hasta;
        public DateTime Hasta
        {
            get { return _hasta; }
            set {
                if (value.Year == 1753) value = DateTime.MinValue;
                _hasta = value; }
        }
        private string _causa;
        public string Causa
        {
            get { return _causa; }
            set { _causa = value; }
        }
        #region IXafEntityObject members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIXafEntityObjecttopic.aspx)
        void IXafEntityObject.OnCreated()
        {
            // Place the entity initialization code here.
            // You can initialize reference properties using Object Space methods; e.g.:
            // this.Address = objectSpace.CreateObject<Address>();
        }
        void IXafEntityObject.OnLoaded()
        {
            // Place the code that is executed each time the entity is loaded here.
        }
        void IXafEntityObject.OnSaving()
        {
            // Place the code that is executed each time the entity is saved here.
        }
        #endregion

        #region IObjectSpaceLink members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIObjectSpaceLinktopic.aspx)
        // Use the Object Space to access other entities from IXafEntityObject methods (see https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113707.aspx).
        IObjectSpace IObjectSpaceLink.ObjectSpace
        {
            get { return objectSpace; }
            set { objectSpace = value; }
        }
        #endregion

        #region INotifyPropertyChanged members (see http://msdn.microsoft.com/en-us/library/system.componentmodel.inotifypropertychanged(v=vs.110).aspx)
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}