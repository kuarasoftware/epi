﻿//using RUTA_XAF_XPO.Module.BusinessObjects.Configuracion.Maestros;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [XafDefaultProperty("Nombre")]
    public class TipoDocumento : XPObject
    {
        public TipoDocumento(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        public int Identificador
        {
            get
            {
                return Oid;
            }
        }

        // Fields...
        private string _Code;
        private string _Nombre;

        [Size(50)]
        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                SetPropertyValue("Nombre", ref _Nombre, value);
            }
        }

        [Size(10)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                SetPropertyValue("Code", ref _Code, value);
            }
        }

        public static System.Data.DataTable fncObtenerListaTipoDocumento_DT()
        {
            System.Data.DataTable dt = new System.Data.DataTable();

            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                string query = "SELECT OID, Nombre, Code FROM TipoDocumento";
                System.Data.SqlClient.SqlDataAdapter sqlAdapter = new System.Data.SqlClient.SqlDataAdapter(query, con);
                sqlAdapter.Fill(dt);
            }

            return dt;
        }
    }
}