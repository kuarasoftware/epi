﻿using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [Description("Aptitudes")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Reconocimientos : XPObject, IXafEntityObject, IEvent
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.).
        #region variables
        private Citas _Citas;
        private Calificacion _Calificacion;
        private DateTime _Fecha;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        private int _Periodicidad;
        private TipoReconocimiento _TipoReconocimiento;
        private bool _AllDay;
        private int _Label;
        private int _Type;
        private string _ResourceId;
        private string _Subject;
        private string _Location;
        private int _Status;
        private Guid _Evento;
        #endregion


        #region GET&SET
        public Citas Citas
        {
            get => _Citas;
            set => SetPropertyValue(nameof(Citas), ref _Citas, value);
        }
       
        [DevExpress.Xpo.DisplayName("Aptitud")]
        public Calificacion Calificacion
        {
            get => _Calificacion;
            set => SetPropertyValue(nameof(Calificacion), ref _Calificacion, value);
        }

        [DevExpress.Xpo.DisplayName("Finalidad Consulta")]
        public TipoReconocimiento TipoReconocimiento
        {
            get => _TipoReconocimiento;
            set
            {
                if (value != null && value.Oid != -1)
                {
                    SetPropertyValue(nameof(TipoReconocimiento), ref _TipoReconocimiento, this.Session.GetObjectByKey<TipoReconocimiento>(value.Oid));
                }
                else
                {
                    SetPropertyValue(nameof(TipoReconocimiento), ref _TipoReconocimiento, value);
                }
            }
        }
        [DevExpress.Xpo.DisplayName("Fecha reconocimiento")]
        [Description("Fecha reconocimiento")]
        public DateTime Fecha
        {
            get => _Fecha;
            set => SetPropertyValue(nameof(Fecha), ref _Fecha, value);
        }
        public int Periodicidad
        {
            get => _Periodicidad;
            set => SetPropertyValue(nameof(Periodicidad), ref _Periodicidad, value);
        }
        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }


        public Guid Evento
        {
            get => _Evento;
            set => SetPropertyValue(nameof(Evento), ref _Evento, value);
        }
        public string Subject
        {
            get
            {
                if (!IsLoading)
                {
                    var nombrecompleto = nombreCompleto() + " {Reconocimientos}";
                    return nombrecompleto;
                }
                return "{Reconocimientos}";
            }
            set
            {

                SetPropertyValue("Subject", ref _Subject, value);


            }
        }

        [Browsable(false)]
        private DateTime _FechaFin;

        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }

        private string nombreCompleto()
        {
            if (Empleado == null) return string.Empty;
            return (string.IsNullOrEmpty(Empleado.NombreCompleto)) ? string.Empty : Empleado.NombreCompleto;
        }
        [Browsable(false)]
        public string Description { get => _Observaciones; set => SetPropertyValue("Descripcion", ref _Observaciones, value); }
        public DateTime StartOn { get => _Fecha; set => SetPropertyValue(nameof(Fecha), ref _Fecha, value); }
        public DateTime EndOn { get => _Fecha; set => SetPropertyValue(nameof(Fecha), ref _Fecha, _Fecha); }
        [Browsable(false)]
        public bool AllDay { get => false; set => SetPropertyValue("AllDay", ref _AllDay, value); }
        public string Location { get => _Location; set => SetPropertyValue("Location", ref _Location, value); }
        public int Label { get => _Label; set => SetPropertyValue("Label", ref _Label, value); }
        public int Status { get => _Status; set => SetPropertyValue("status", ref _Status, value); }
        public int Type { get => _Type; set => SetPropertyValue("Type", ref _Type, value); }
        public string ResourceId { get => _ResourceId; set => SetPropertyValue("ResourceId", ref _ResourceId, value); }

        public object AppointmentId { get { return Oid; } }

        #endregion
        #region METODOS
        public Reconocimientos(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Label = 2;
        }
        public void OnCreated()
        {
        }
        void IXafEntityObject.OnLoaded()
        {
            if (StartOn <= DateTime.Now.Date && Calificacion == null)
            {
                Label = 1;
            }
        }
        protected override void OnDeleting()
        {
                XPCollection<Ofrecimientos> ofrecimientosaborrar = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Reconocimiento.Oid] =?", this.Oid));
                if ((ofrecimientosaborrar.Count != 0) & (ofrecimientosaborrar != null))
                {
                    if (MessageBox.Show("Este empleado tiene un ofrecimiento. Lo borramos ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        
                    string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                    string strsql = "Delete Ofrecimientos Where Reconocimientos=" + this.Oid.ToString();
                    using (SqlConnection con = new SqlConnection(contection))
                    {
                        //ofr.Session.ExecuteQuery(strsql);
                        con.Open();

                        //Iniciamos una transaccion local.
                        SqlTransaction sqlTran = con.BeginTransaction();

                        SqlCommand cmd = new SqlCommand(strsql, con);

                        cmd.Transaction = sqlTran;
                        cmd.CommandText = strsql;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        sqlTran.Commit();
                        con.Close();
                    }
                }
                }
            //Session.CommitTransaction();
           
        }

        void IXafEntityObject.OnSaving()
        {
            Event @event;
            if (this.Evento != Guid.Empty)
            {
                @event = this.Session.GetObjectByKey<Event>(this.Evento);
            }
            else
            {
                @event = new Event(this.Session);
            }
            if (@event != null)
            {
                @event.AllDay = this.AllDay;
                @event.Description = this.Description;
                @event.Label = this.Label;
                @event.Location = this.Location;
                @event.StartOn = this.StartOn;
                @event.EndOn = this.EndOn;
                @event.Status = this.Status;
                @event.Subject = this.Subject;
                @event.Type = this.Type;
                @event.Save();
                this.Evento = @event.Oid;
                this.Save();
                if (IsDeleted) @event.Delete();
            
            }
        }
        protected override void OnSaved()
        {
            
            var a = this.Oid;
            XPCollection<Ofrecimientos> ofrecimientosant = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Reconocimiento.Oid] =?", this.Oid));

            if ((this.Periodicidad >= 0) && (ofrecimientosant.Count==0))
            {
                DateTime fecha = this.Fecha;
                
                bool pAceptado = false;
                if ((this.Calificacion.Oid == 4) || (this.Calificacion.Oid == 5)) pAceptado = true;
                //if (pAceptado)
                    fecha = fecha.AddMonths(this.Periodicidad);
                //if (! pAceptado) fecha =this.Citas.Ofrecimientos.Fecha.AddMonths(Periodicidad);
                if (fecha.DayOfWeek == DayOfWeek.Saturday)
                {
                    fecha = fecha.AddDays(2);
                }
                else if (fecha.DayOfWeek == DayOfWeek.Sunday)
                {
                    fecha = fecha.AddDays(1);
                }
                if (pAceptado)
                {
                    Ofrecimientos ofrecimiento = new Ofrecimientos(this.Session)
                    {
                        Description = this.Description,
                        Obsevaciones = this.Obsevaciones,
                        Fecha = fecha,
                        Empleado = this.Empleado,
                        Periodicidad = this.Periodicidad,
                        TipoOfrecimiento = this.Session.GetObjectByKey<BusinessObjects.TipoOfrecimiento>(1),
                        Aceptado = pAceptado,
                        Reconocimiento = this,
                        Periodico=true,
                        Automatico=true
                    };
                    ofrecimiento.Save();
                }
                else
                {
                    Ofrecimientos ofrecimiento = new Ofrecimientos(this.Session)
                    {
                        
                        Description = this.Description,
                        Obsevaciones = this.Obsevaciones,
                        Fecha = fecha,
                        Empleado = this.Empleado,
                        Periodicidad = this.Periodicidad,
                        TipoOfrecimiento = this.Session.GetObjectByKey<BusinessObjects.TipoOfrecimiento>(1),
                        Reconocimiento = this,
                        Periodico =true,
                        Automatico=true
                    };
                    ofrecimiento.Save();
                }
                this.Session.CommitTransaction();
            }
        }
        #endregion
    }
}