﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Solicitudes : XPObject, IXafEntityObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private DateTime _Fecha;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        private string _Description;
        private bool _CitaRecibida;
        private Ofrecimientos _Ofrecimiento;
        private Citas _Cita;
        private bool _Comunicadoalamutua;
        #endregion
        #region GET&SET

        [Description("Comunicado a la mutua")]
        public bool Comunicadoalamutua
        {
            get { return _Comunicadoalamutua; }
            set { SetPropertyValue(nameof(Comunicadoalamutua), ref _Comunicadoalamutua, value); }
        }
        public bool CitaRecibida
        {
            get { return _CitaRecibida; }
            set { _CitaRecibida = value;
                if (!IsLoading)
                {
                   try
                    {
                        Ofrecimiento.CitaRecibida = value;
                        Ofrecimiento.Automatico = true;
                        Ofrecimiento.Save();
                    }
                    catch{ }
                }
            }
        }
        public Ofrecimientos Ofrecimiento
        {
            get { return _Ofrecimiento; }
            set { SetPropertyValue(nameof(Ofrecimiento), ref _Ofrecimiento, value); }
        }
        public Citas Cita
        {
            get { return _Cita; }
            set { SetPropertyValue(nameof(Cita), ref _Cita, value); }
        }
        [Browsable(false)]
        public DateTime Fecha
        {
            get
            {
                return _Fecha;
            }
            set
            {
                if (_Fecha == value)
                {
                    return;
                }

                SetPropertyValue(nameof(Fecha), ref _Fecha, value);
            }
        }

      

       

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }

        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }

     
        private string nombreCompleto()
        {
            if (Empleado == null) return string.Empty;
            return (string.IsNullOrEmpty(Empleado.NombreCompleto)) ? string.Empty : Empleado.NombreCompleto;
        }
       

        [Browsable(false)]
        public string Description { get => _Description; set => SetPropertyValue("Descripcion", ref _Description, value); }
       
        #endregion
        public Solicitudes(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        public void OnCreated()
        {
            //throw new NotImplementedException();
        }

        void IXafEntityObject.OnLoaded()
        {
            
        }

        void IXafEntityObject.OnSaving()
        {
            
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}