﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DomainComponent]
    [DefaultClassOptions]
    public class EmpleadosVerifica : IXafEntityObject, IObjectSpaceLink, INotifyPropertyChanged
    {
        private IObjectSpace objectSpace;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public EmpleadosVerifica()
        {
        }
        [Browsable(false)]  // Hide the entity identifier from UI.
        public int ID { get; set; }

        private BusinessObjects.Temel.Employee _Empleado;

        public BusinessObjects.Temel.Employee Empleado
        {
            get { return _Empleado; }
            set { _Empleado = value; }
        }

        
        private DateTime _fechafinalcontrato;
            [Description("Fecha final contrato")]
        public DateTime fechafinalcontrato
        
        {
            get { return _fechafinalcontrato; }
            set {
                if (value.Year == 1753) value = DateTime.MinValue;
                _fechafinalcontrato = value;
            }
        }
        private DateTime _ultimoofrecimiento;
        [Description("Fecha Último ofrecimiento")]
        public DateTime ultimoofrecimiento

        {
            get { return _ultimoofrecimiento; }
            set
            { if (value.Year == 1900) value = DateTime.MinValue;
              _ultimoofrecimiento = value;
            }
        }
        private Citas _ultimacita;
        [Description("Última cita")]
        public Citas ultimacita

        {
            get { return _ultimacita; }
            set
            {
                _ultimacita = value;
            }
        }
        private Reconocimientos _ultimoreconocimiento;
        [Description("Último reconocimiento")]
        public Reconocimientos ultimoreconocimiento

        {
            get { return _ultimoreconocimiento; }
            set
            {
                _ultimoreconocimiento = value;
            }
        }
        private Ofrecimientos _proximoofrecimiento;
        [Description("Próximo ofrecimiento")]
        public Ofrecimientos proximoofrecimiento

        {
            get { return _proximoofrecimiento; }
            set
            {
                _proximoofrecimiento = value;
            }
        }

        #region IXafEntityObject members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIXafEntityObjecttopic.aspx)
        void IXafEntityObject.OnCreated()
        {
            // Place the entity initialization code here.
            // You can initialize reference properties using Object Space methods; e.g.:
            // this.Address = objectSpace.CreateObject<Address>();
        }
        void IXafEntityObject.OnLoaded()
        {
            // Place the code that is executed each time the entity is loaded here.
        }
        void IXafEntityObject.OnSaving()
        {
            // Place the code that is executed each time the entity is saved here.
        }
        #endregion

        #region IObjectSpaceLink members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIObjectSpaceLinktopic.aspx)
        // Use the Object Space to access other entities from IXafEntityObject methods (see https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113707.aspx).
        IObjectSpace IObjectSpaceLink.ObjectSpace
        {
            get { return objectSpace; }
            set { objectSpace = value; }
        }
        #endregion

        #region INotifyPropertyChanged members (see http://msdn.microsoft.com/en-us/library/system.componentmodel.inotifypropertychanged(v=vs.110).aspx)
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}