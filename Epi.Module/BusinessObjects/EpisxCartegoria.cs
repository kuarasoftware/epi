﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Epi.Module.BusinessObjects.Temel;
using System;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("EmployeeStatisticsGroup")]
    public class EpisxCartegoria : XPObject
    {
        public EpisxCartegoria(Session session)
                   : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        EmployeeStatisticsGroup _Categoria = null;
        public EmployeeStatisticsGroup Categoria
        {
            get { return _Categoria; }
            set
            {
                if (_Categoria == value)
                    return;

                // Store a reference to the former _Categoria. 
                EmployeeStatisticsGroup prevCategoria = _Categoria;
                _Categoria = value;

                if (IsLoading) return;

                // Remove an _Categoria's reference to this building, if exists. 
                if (prevCategoria != null && prevCategoria.EpisxCartegoria == this)
                    prevCategoria.EpisxCartegoria = null;

                // Specify that the building is a new _Categoria's house. 
                if (_Categoria != null)
                    _Categoria.EpisxCartegoria = this;
                OnChanged("Categoria");
            }
        }


        [Association("Categoria-Epis")]
        public XPCollection<EpisCategoriaCantidad> EpisCategoriaCantidad
        {
            get { return GetCollection<EpisCategoriaCantidad>("EpisCategoriaCantidad"); }
        }

    }
}