﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using DevExpress.ExpressApp.DC;
using DevExpress.Xpo;
using System;
using System.Windows.Forms;

namespace Epi.Module.BusinessObjects.Temel
{
    [XafDefaultProperty("Description")]
    [Persistent(@"Employee Statistics Group")]
    public partial class EmployeeStatisticsGroup : XPLiteObject
    {
        string fCode;
        [Key]
        [Size(10)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>(nameof(Code), ref fCode, value); }
        }
        string fDescription;
        [Size(50)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>(nameof(Description), ref fDescription, value); }
        }


        EpisxCartegoria episxCartegoria = null;
        public EpisxCartegoria EpisxCartegoria
        {
            get { return episxCartegoria; }
            set
            {
                if (episxCartegoria == value)
                    return;

                // Store a reference to the person's former episxCartegoria. 
                EpisxCartegoria prevEpisxCartegoria = episxCartegoria;
                episxCartegoria = value;

                if (IsLoading) return;

                // Remove a reference to the episxCartegoria's owner, if the person is its owner. 
                if (prevEpisxCartegoria != null && prevEpisxCartegoria.Categoria == this)
                {
                    MessageBox.Show("Esta categoria ya estaba creada, será eliminada, para que no sea eliminada presione cancelar en la parte superior.", "Alerta!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    
                        prevEpisxCartegoria.Delete();
                        prevEpisxCartegoria.Save();
                    
                }

                // Specify the person as a new owner of the episxCartegoria. 
                if (episxCartegoria != null)
                    episxCartegoria.Categoria = this;

                OnChanged("EpisxCartegoria");
            }
        }
    }
}
