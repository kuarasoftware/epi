﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Festivos")]

    public class Festivos : XPObject, IXafEntityObject, IEvent
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private DateTime _Fecha;
        private DateTime _FechaFin;
        private string _Observaciones;
        private bool _AllDay;
        private int _Label;
        private int _Type;
        private string _ResourceId;
        private string _Subject;
        private string _Location;
        private int _Status;
        private Guid _Evento;
        #endregion

        #region GET&SET


        public DateTime Fecha
        {
            get => _Fecha;
            set => SetPropertyValue(nameof(Fecha), ref _Fecha, value);
        }
        public DateTime FechaFin
        {
            get => _FechaFin;
            set => SetPropertyValue(nameof(FechaFin), ref _FechaFin, value);
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        public Guid Evento
        {
            get => _Evento;
            set => SetPropertyValue(nameof(Evento), ref _Evento, value);
        }
        public string Subject
        {
            get
            {
                if (!IsLoading)
                {
                    var nombrecompleto = "{Festivo}";
                    return nombrecompleto;
                }
                return "{Festivo}";
            }
            set
            {

                SetPropertyValue("Subject", ref _Subject, value);


            }
        }

        public string Description { get => _Observaciones; set => SetPropertyValue("Descripcion", ref _Observaciones, value); }
        public DateTime StartOn { get => _Fecha; set => SetPropertyValue(nameof(Fecha), ref _Fecha, value); }
        public DateTime EndOn { get => _FechaFin; set => SetPropertyValue(nameof(FechaFin), ref _FechaFin, _Fecha); }
        [Browsable(false)]
        public bool AllDay { get => true; set => SetPropertyValue("AllDay", ref _AllDay, value); }
        public string Location { get => _Location; set => SetPropertyValue("Location", ref _Location, value); }
        public int Label { get => _Label; set => SetPropertyValue("Label", ref _Label, value); }
        public int Status { get => _Status; set => SetPropertyValue("status", ref _Status, value); }
        public int Type { get => _Type; set => SetPropertyValue("Type", ref _Type, value); }
        public string ResourceId { get => _ResourceId; set => SetPropertyValue("ResourceId", ref _ResourceId, value); }

        public object AppointmentId { get { return Oid; } }

        #endregion
        #region METODOS
        public Festivos(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Label = 1;
        }
        public void OnCreated()
        {

        }
        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {
            Event @event;
            if (this.Evento != Guid.Empty)
            {
                @event = this.Session.GetObjectByKey<Event>(this.Evento);
            }
            else
            {
                @event = new Event(this.Session);
            }
            if (@event != null)
            {
                @event.AllDay = this.AllDay;
                @event.Description = this.Description;
                @event.EndOn = this.EndOn;
                @event.Label = this.Label;
                @event.Location = this.Location;
                @event.StartOn = this.StartOn;
                @event.Status = this.Status;
                @event.Subject = this.Subject;
                @event.Type = this.Type;
                @event.Save();
                this.Evento = @event.Oid;
                this.Save();
            }
        }
        #endregion
    }
}