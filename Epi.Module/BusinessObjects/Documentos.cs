﻿//using RUTA_XAF_XPO.Module.BusinessObjects.Configuracion.Maestros;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Documento : XPObject, IXafEntityObject
    {
        #region VARIABLES
        #region public
        public const string ExtensionDocx = ".docx";
        public const string ExtensionPdf = ".pdf";
        #endregion
        #region private
        private bool _Anexar;
        private long _OidTabla;
        private string _Tabla;
        private string _Extension;
        private string _RutaFisica;
        private string _Descripcion;
        private TipoDocumento _TipoDocumento;
        private DateTime _FechaModif;
        private DateTime _FechaAltaDate;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        #endregion

        #endregion VARIABLES
        #region GET & SET        
        public TipoDocumento TipoDocumento
        {
            get => _TipoDocumento;
            set
            {
                _TipoDocumento = value;
            }
        }
        public string FechaAlta
        {
            get
            {
                if (FechaModif.ToString() == "01/01/0001 0:00:00")
                {
                    return string.Empty;
                }
                return FechaAltaDate.ToString();
            }
        }
        [Browsable(false)]
        public DateTime FechaAltaDate
        {
            get => _FechaAltaDate;
            set => SetPropertyValue(nameof(FechaAltaDate), ref _FechaAltaDate, value);
        }
        public string FechaUltimaModificacion
        {
            get
            {
                if (FechaModif.ToString() == "01/01/0001 0:00:00")
                {
                    return string.Empty;
                }
                return FechaModif.ToString();
            }
        }
        [Browsable(false)]
        public DateTime FechaModif
        {
            get => _FechaModif;
            set => SetPropertyValue(nameof(FechaModif), ref _FechaModif, value);
        }
        [Size(100)]
        public string Descripcion
        {
            get => _Descripcion;
            set => SetPropertyValue(nameof(Descripcion), ref _Descripcion, value);
        }
        public string RutaFisica
        {
            get => _RutaFisica;
            set => SetPropertyValue(nameof(RutaFisica), ref _RutaFisica, value);
        }
        public byte[] IDOCUMENTO
        {
            get { return GetPropertyValue<byte[]>("IDOCUMENTO"); }
            set { SetPropertyValue<byte[]>("IDOCUMENTO", value); }
        }
        [Size(50)]
        public string Extension
        {
            get => _Extension;
            set => SetPropertyValue(nameof(Extension), ref _Extension, value);
        }
        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }
        /// <summary>
        /// Anexar si es true se puede insertar en la bbdd
        /// </summary>
        public bool Anexar
        {
            get => _Anexar;
            set => SetPropertyValue(nameof(Anexar), ref _Anexar, value);
        }
        public string Tabla
        {
            get => _Tabla;
            set => SetPropertyValue(nameof(Tabla), ref _Tabla, value);
        }

        public long OidTabla
        {
            get => _OidTabla;
            set => SetPropertyValue(nameof(OidTabla), ref _OidTabla, value);
        }
        #endregion GET & SET
        #region METODOS
        public Documento(Session session)
        : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Tabla = Controllers.BotonesDocumentos.TABLADOCUMENTO;
            OidTabla = Controllers.BotonesDocumentos.OIDDOCUMENTO;
        }

        public void OnCreated()
        {

        }

        public void OnLoaded()
        {
        }
        public void OnSaving()
        {
            if (!Anexar)
            {
                IDOCUMENTO = null;
            }
        }
        //public static byte[] GetOpenXmlBytesPlantilla(string path)
        //{
        //    byte[] docByte = null;
        //    using (RichEditDocumentServer plantilla = new RichEditDocumentServer())
        //    {
        //        plantilla.LoadDocument(path);
        //        //recojemos la plantilla
        //        docByte = plantilla.OpenXmlBytes;
        //    }
        //    return docByte;
        //}
        public static bool EliminarFichero(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                return true;
            }
            catch (Exception e)
            {

                return false;
            }
        }
        #endregion METODOS
    }
}
