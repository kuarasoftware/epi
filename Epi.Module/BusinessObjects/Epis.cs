﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Descripcion")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Epis : XPObject, IXafEntityObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.).
        #region variables
        private string _Descripcion;
        private string _Observaciones;
        #endregion
        #region GET&SET
        [RuleRequiredField("RuleRequiredField for Epi.Descripcion", DefaultContexts.Save, "Debe especificar una descripcion", SkipNullOrEmptyValues = false)]
        public string Descripcion
        {
            get => _Descripcion;
            set => SetPropertyValue(nameof(Descripcion), ref _Descripcion, value);
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        [Size(1024)]
        [ImageEditor]
        public byte[] Imagen
        {
            get { return GetPropertyValue<byte[]>("Imagen"); }
            set { SetPropertyValue<byte[]>("Imagen", value); }
        }
        #endregion
        #region Colecciones

        //[Association("EpisxEmpleado-Epis")]
        //public XPCollection<EpisxEmpleado> EpisxEmpleados
        //{
        //    get
        //    {
        //        return GetCollection<EpisxEmpleado>("EpisxEmpleados");
        //    }
        //}
        private TextOnlyEnum _TipoEpi;

        public TextOnlyEnum TipoEpi
        {
            get { return _TipoEpi; }
            set { SetPropertyValue("Estado", ref _TipoEpi, value); }
        }

        public enum TextOnlyEnum { Uniformidad, Epi };

        private Marca _Marca;

        public Marca Marca
        {
            get { return _Marca; }
            set { _Marca = value; }
        }

        private Modelo _Modelo;

        public Modelo Modelo
        {
            get { return _Modelo; }
            set { _Modelo = value; }
        }


        [Association("Epis-EpisxEmpleado"), Browsable(false)]
        public IList<EpisxEmpleadoEpis> EpisxEmpleadoEpis
        {
            get
            {
                return GetList<EpisxEmpleadoEpis>("EpisxEmpleadoEpis");
            }
        }

        [ManyToManyAlias("EpisxEmpleadoEpis", "EpisxEmpleado")]
        public IList<EpisxEmpleado> EpisxEmpleado
        {
            get
            {
                return GetList<EpisxEmpleado>("EpisxEmpleado");
            }
        }
        #endregion
        #region METODOS
        public Epis(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        public void OnCreated()
        {
        }

        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {

        }

        #endregion
    }
}