﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using Epi.Module.Utils;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Fecha")]

    public class Ofrecimientos : XPObject, IXafEntityObject, IEvent
    {
        #region variables
        private bool _Calificacion;
        private TipoReconocimiento _FinalidadOfrecimiento;
        private DateTime _FechaEntrega;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        private string _Description;
        private int _Periodicidad;
        private string _Subject;
        private string _Location;
        private bool _AllDay;
        private int _Label;
        private int _Type;
        private string _ResourceId;
        private Guid _Evento;
        private bool _ComumicadoSMS;
        private bool _CitaRecibida;
        private TipoOfrecimiento _TipoOfrecimiento;
        private bool _Aceptado;
        private bool _Rechazado;
        private bool _InformeSolicitado;
        private bool _Impreso;
        private bool _NuevaContratacion;
        private bool _Periodico;
        private Reconocimientos _Reconocimientos;
        private bool _Automatico;
        private bool Borrando = false;
        #endregion


        #region GET&SET



        public bool Aceptado
        {
            get { return _Aceptado; }
            set
            {
                if (_Aceptado == value)
                {
                    return;
                }

                _Aceptado = value;

                if (value == true)
                    NoAceptado = false;
            }
        }
        public bool Automatico
        {
            get { return _Automatico; }
            set { _Automatico = value; }
        }
        [Browsable(false)]
        public bool CitaRecibida
        {
            get { return _CitaRecibida; }
            set {
                if (Automatico) _CitaRecibida = value; }
        }
        [Description("Finalidad ofrecimiento")]
        public TipoReconocimiento FinalidadOfrecimiento
        {
            get { return _FinalidadOfrecimiento; }
            set { _FinalidadOfrecimiento = value; }
        }
        [Description("Periódico")]
        public bool Periodico
        {
            get { return _Periodico; }
            set { _Periodico = value; }
        }
        [Description("Nueva contratoación")]
        public bool NuevaContratacion
        {
            get { return _NuevaContratacion; }
            set {
                if ((!IsLoading) & (value == true)) Aceptado = true;
                _NuevaContratacion = value;
            }
        }
        public bool Impreso
        {
            get { return _Impreso; }
            set { _Impreso = value; }
        }
        [Browsable(false)]
        public bool SolicitudRealizada
        {
            get { return _InformeSolicitado; }
            set { _InformeSolicitado = value; }
        }

        [Browsable(false)]
        public Reconocimientos  Reconocimiento
        {
            get { return _Reconocimientos; }
            set { _Reconocimientos = value; }
        }
        public bool ComumicadoSMS
        {
            get { return _ComumicadoSMS; }
            set { _ComumicadoSMS = value; }
        }


        public bool NoAceptado
        {
            get { return _Rechazado; }

            set
            {
                if (_Rechazado == value)
                {
                    return;
                }

                _Rechazado = value;

                if (value == true)
                Aceptado = false;
            }

        }

        public TipoOfrecimiento TipoOfrecimiento
        {
            get => _TipoOfrecimiento;
            set => SetPropertyValue(nameof(TipoOfrecimiento), ref _TipoOfrecimiento, value);
        }
        public bool Calificacion
        {
            get => _Calificacion;
            set => SetPropertyValue(nameof(Calificacion), ref _Calificacion, value);
        }
        [DevExpress.Xpo.DisplayName("Fecha entrega ofrecimiento")]
        [Description("Fecha entrega ofrecimiento")]
        public DateTime Fecha
        {
            get
            {
                if (!IsLoading)
                {
                    FechaFin = _FechaEntrega;
                }
                return _FechaEntrega;
            }
            set
            {
                if (_FechaEntrega == value)
                {
                    return;
                }

                SetPropertyValue(nameof(Fecha), ref _FechaEntrega, value);
            }
        }

        [Browsable(false)]
        private DateTime _FechaFin;

        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }


        public int Periodicidad
        {
            get => _Periodicidad;
            set => SetPropertyValue(nameof(Periodicidad), ref _Periodicidad, value);
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }

        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set
            {
                if (!IsLoading)
                {
                    if ((value.Activo == false) & (value!=null))
                    {
                        if (MessageBox.Show("Este empleado no tienen contrato actico. Quiere continuar ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                        {
                            //value = null;
                            _Empleado = null;
                            value = null;
                            SetPropertyValue(nameof(Empleado), ref _Empleado, value);
                            
                            
                        }
                    }
                }
                SetPropertyValue(nameof(Empleado), ref _Empleado, value);
            }

        }

        public string Subject
        {
            get
            {
                if (!IsLoading)
                {
                    var nombrecompleto = nombreCompleto() + " {Ofrecimientos}";
                    return nombrecompleto;
                }
                return "{Ofrecimientos}";
            }
            set
            {

                SetPropertyValue("Subject", ref _Subject, value);


            }
        }

        private string nombreCompleto()
        {
            if (Empleado == null) return string.Empty;
            return (string.IsNullOrEmpty(Empleado.NombreCompleto)) ? string.Empty : Empleado.NombreCompleto;
        }
        public Guid Evento
        {
            get => _Evento;
            set => SetPropertyValue(nameof(Evento), ref _Evento, value);
        }

        [Browsable(false)]
        public string Description { get => _Description; set => SetPropertyValue("Descripcion", ref _Description, value); }
        public DateTime StartOn
        {
            get
            {
                return _FechaEntrega;
            }
            set
            {
                if (_FechaEntrega == value)
                {
                    return;
                }

                SetPropertyValue(nameof(Fecha), ref _FechaEntrega, value);
            }
        }
        public DateTime EndOn { get => _FechaEntrega; set => SetPropertyValue(nameof(_FechaEntrega), ref _FechaEntrega, _FechaEntrega); }
        [Browsable(false)]
        public bool AllDay { get => false; set => SetPropertyValue("AllDay", ref _AllDay, value); }
        public string Location { get => _Location; set => SetPropertyValue("Location", ref _Location, value); }
        public int Label { get => _Label; set => SetPropertyValue("Label", ref _Label, value); }
        public int Status { get => _Periodicidad; set => SetPropertyValue(nameof(Periodicidad), ref _Periodicidad, value); }
        public int Type { get => _Type; set => SetPropertyValue("Type", ref _Type, value); }
        public string ResourceId { get => _ResourceId; set => SetPropertyValue("ResourceId", ref _ResourceId, value); }

        public object AppointmentId { get { return Oid; } }


        #endregion
        #region METODOS
        public Ofrecimientos(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Label = 3;
            Periodicidad = 12;
        }
        public void OnCreated()
        {
        }
        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {
            try
            {
                if (!IsLoading) 
                {
                    if (Empleado==null)
                    {
                        this.Delete();
                        return;
                    }
                    //comprueba otros ofrecimientos
                    XPCollection<Ofrecimientos> ofrecimientosant = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Oid] <> ? And [Empleado] = ? And ([Fecha] >= ? AND [Fecha]<= ?)", this.Oid, this.Empleado, this.Fecha.AddMonths(-3),this.Fecha.AddMonths(+3)));
                    if ((ofrecimientosant.Count != 0) && (ofrecimientosant!=null) &&  (Reconocimiento != null))
                    {
                        if (MessageBox.Show("Este empleado ya tiene otro ofrecimiento para estos últimos meses. Quiere continuar ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No )
                        {
                            try
                            {
                                this.Delete();
                            }
                            catch
                            {

                            }
                            return;
                        }
                    }
                    if (TipoOfrecimiento == this.Session.GetObjectByKey<TipoOfrecimiento>(1))
                    {
                        if (((NoAceptado) | (!Aceptado)) && (this.NuevaContratacion))
                        {
                            MessageBox.Show("Este empleado es de nueva contratación debes aceptar el reconocimiento médico", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            NoAceptado = false;
                            Aceptado = true;
                        }

                    }
                    if (ComumicadoSMS == false && Fecha.AddDays(-30) <= DateTime.Now)
                    {
                        EnvioSMS envioSMS = new EnvioSMS();
                        Ofrecimientos Ofre = this;
                        envioSMS.enviaSMS(ref Ofre, this.Empleado.MobilePhoneNo, "Tiene una revisión médica Dia" + this.Fecha.ToShortDateString());

                    }
                    Event @event;
                    if (this.Evento != Guid.Empty)
                    {
                        @event = this.Session.GetObjectByKey<Event>(this.Evento);
                    }
                    else
                    {
                        @event = new Event(this.Session);
                    }
                    if (@event != null)
                    {
                        @event.AllDay = this.AllDay;
                        @event.Description = this.Description;
                        @event.EndOn = this.Fecha;
                        @event.Label = this.Label;
                        @event.Location = this.Location;
                        @event.StartOn = this.Fecha;
                        @event.Status = this.Status;
                        @event.Subject = this.Subject;
                        @event.Type = this.Type;
                        @event.Save();
                        this.Evento = @event.Oid;
                        // this.Save();
                        if (IsDeleted) @event.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("Error al enviar SMS. No hay la información necesaria en el empleado.");
            }
            
        }
        protected override void OnDeleting()
        {
            if (Borrando == false)
            {
                Borrando = true;
                XPCollection<Solicitudes> solicitudesaborrar = new XPCollection<Solicitudes>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimiento.Oid] =?", this.Oid));
                if ((solicitudesaborrar.Count != 0) & (solicitudesaborrar != null))
                {
                    if (MessageBox.Show("Este empleado tiene una solicitud. La borramos ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                        string strsql = "Delete Solicitudes Where Ofrecimiento=" + this.Oid.ToString();
                        using (SqlConnection con = new SqlConnection(contection))
                        {
                            //ofr.Session.ExecuteQuery(strsql);
                            con.Open();

                            //Iniciamos una transaccion local.
                            SqlTransaction sqlTran = con.BeginTransaction();

                            SqlCommand cmd = new SqlCommand(strsql, con);

                            cmd.Transaction = sqlTran;
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            sqlTran.Commit();
                            con.Close();
                        }

                    }
                }
                XPCollection<Citas> Citassaborrar = new XPCollection<Citas>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimientos.Oid] =?", this.Oid));
                if ((Citassaborrar.Count != 0) & (Citassaborrar != null))
                {
                    if (MessageBox.Show("Este empleado tiene una cita. La borramos ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (Citas citaaborra in Citassaborrar)
                        {
                            // Borrar reconocimientos
                            // Citassaborrar[t].Delete();
                            string contection2 = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                            string strsql2 = "Delete Reconocimientos Where Citas=" + citaaborra.Oid.ToString();
                            using (SqlConnection con = new SqlConnection(contection2))
                            {
                                //ofr.Session.ExecuteQuery(strsql);
                                con.Open();

                                //Iniciamos una transaccion local.
                                SqlTransaction sqlTran = con.BeginTransaction();

                                SqlCommand cmd = new SqlCommand(strsql2, con);

                                cmd.Transaction = sqlTran;
                                cmd.CommandText = strsql2;
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                sqlTran.Commit();
                                con.Close();
                            }
                        }
                        // Borrar Citas
                        string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                        string strsql = "Delete Citas Where Ofrecimientos=" + this.Oid.ToString();
                        using (SqlConnection con = new SqlConnection(contection))
                        {
                            //ofr.Session.ExecuteQuery(strsql);
                            con.Open();

                            //Iniciamos una transaccion local.
                            SqlTransaction sqlTran = con.BeginTransaction();

                            SqlCommand cmd = new SqlCommand(strsql, con);

                            cmd.Transaction = sqlTran;
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            sqlTran.Commit();
                            con.Close();
                        }
                    }
                }

                // Session.CommitTransaction();
            }

        }
        protected override void OnSaved()
        {
            if (Aceptado==true)
            {
                try
                {
                    var a = this.Oid;
                    XPCollection<Solicitudes> solicitudant = new XPCollection<Solicitudes>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimiento.Oid] =?", this.Oid));
                    if ((solicitudant.Count != 0) & (solicitudant != null))
                    {
                        string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                        string strsql = "Delete Solicitudes Where Ofrecimiento=" + this.Oid.ToString();
                        using (SqlConnection con = new SqlConnection(contection))
                        {
                            //ofr.Session.ExecuteQuery(strsql);
                            con.Open();

                            //Iniciamos una transaccion local.
                            SqlTransaction sqlTran = con.BeginTransaction();

                            SqlCommand cmd = new SqlCommand(strsql, con);

                            cmd.Transaction = sqlTran;
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            sqlTran.Commit();
                            con.Close();
                        }

                    }
                    Solicitudes solicitudes = new Solicitudes(this.Session);
                    solicitudes.CitaRecibida = false;
                    solicitudes.Description = Description;
                    solicitudes.Empleado = Empleado;
                    solicitudes.Fecha = DateTime.Now;
                    solicitudes.Obsevaciones = Obsevaciones;
                    solicitudes.Ofrecimiento = this;
                    solicitudes.Save();
                    SolicitudRealizada = true;
                }
                catch { }
            }
            if ((NoAceptado == true) && (Periodicidad >= 0))
                {
                try
                {
                    XPCollection<Ofrecimientos> DeleteOfrecimientos = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Oid] <> ? And [Empleado] = ? And [Fecha]>?", this.Oid, this.Empleado, this.Fecha));
                    XPCollection<Ofrecimientos> DeleteDOfrecimientos = new XPCollection<Ofrecimientos>(this.Session, false);
                    foreach (Ofrecimientos Ofre in DeleteOfrecimientos)
                    {
                        XPCollection<Citas> DeleteCitas = new XPCollection<Citas>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimientos]=?", Ofre.Oid));
                        if (DeleteCitas.Count == 0)
                        {
                            DeleteDOfrecimientos.Add(Ofre);
                            XPCollection<Solicitudes> solicitudant = new XPCollection<Solicitudes>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimiento.Oid] =?", Ofre.Oid));
                            if ((solicitudant.Count != 0) & (solicitudant != null))
                            {

                                for (int t = 0; t <= solicitudant.Count - 1; t++)
                                {
                                    solicitudant[t].Delete();
                                }
                            }
                        }

                    }
                    while (DeleteDOfrecimientos.Count > 0)
                    {
                        DeleteDOfrecimientos[0].Delete();
                    }
                    DateTime fecha = this.Fecha;
                    if (Periodicidad == 0) Periodicidad = 1;
                    fecha = fecha.AddMonths(Periodicidad);
                    if (fecha.DayOfWeek == DayOfWeek.Saturday)
                    {
                        fecha = fecha.AddDays(2);
                    }
                    else if (fecha.DayOfWeek == DayOfWeek.Sunday)
                    {
                        fecha = fecha.AddDays(1);
                    }
                    Ofrecimientos ofrecimiento = new Ofrecimientos(this.Session)
                    {
                        Description = this.Description,
                        Obsevaciones = this.Obsevaciones,
                        Fecha = fecha,
                        Empleado = this.Empleado,
                        Periodicidad = this.Periodicidad,
                        TipoOfrecimiento = this.TipoOfrecimiento,
                        Aceptado = false,
                        NoAceptado = false,
                        Periodico = this.Periodico,
                        NuevaContratacion = this.NuevaContratacion
                    };
                }
                catch { }
                
            }
            try
            {
                this.Session.CommitTransaction();
            }
            catch { }
            }

        #endregion
    }
}