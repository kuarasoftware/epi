﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;
using static Epi.Module.BusinessObjects.EpisxEmpleadoEpis;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    public class EpisCategoriaCantidad : BaseObject
    {
        public EpisCategoriaCantidad(Session session)
                   : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

        }
        private EpisxCartegoria episxCartegoria;
        [Association("Categoria-Epis"), Browsable(false)]
        public EpisxCartegoria EpisxCartegoria
        {
            get { return episxCartegoria; }
            set { SetPropertyValue("EpisxCartegoria", ref episxCartegoria, value); }
        }
        private Epis epis;

        public Epis Epis
        {
            get { return epis; }
            set { SetPropertyValue("Epis", ref epis, value); }
        }
        private int _Cantidad;

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
        private Boolean _Vigencia;

        public Boolean Vigencia
        {
            get { return _Vigencia; }
            set { _Vigencia = value; }
        }
        private TextOnlyEnum _Estado;

        public TextOnlyEnum Estado
        {
            get { return _Estado; }
            set { SetPropertyValue("Estado", ref _Estado, value); }
        }
    }
}