﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;
namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Descripcion")]
    [Description("Finalidad Ofrtecimiento")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class TipoReconocimiento    : XPObject, IXafEntityObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.).
        #region variables
        private string _Descripcion;

        #endregion
        #region GET&SET
        [RuleRequiredField("RuleRequiredField for TipoReconocimiento.Descripcion", DefaultContexts.Save, "Debe especificar una descripcion", SkipNullOrEmptyValues = false)]
        public string Descripcion
        {
            get => _Descripcion;
            set => SetPropertyValue(nameof(Descripcion), ref _Descripcion, value);
        }

        #endregion

        #region METODOS
        public TipoReconocimiento(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        public void OnCreated()
        {
        }

        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {

        }

        #endregion
    }
}