﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Ofrecimientos")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Citas : XPObject, IXafEntityObject, IEvent
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private Ofrecimientos _ofrecimientos;
        private DateTime _FechaEntrega;
        private DateTime _FechaCita;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        private string _Descripcion;
        private bool _NoPresentado;
        private bool _CitaAnulada;
        private TipoReconocimiento _TipoReconocimiento;
        private bool _AllDay;
        private int _Label;
        private int _Type;
        private string _ResourceId;
        private string _Subject;
        private string _Location;
        private int _Status;
        private Guid _Evento;
        public Boolean _Previs;
        public Boolean _UnidadMovil;
        public bool _Impreso;
        #endregion


        #region GET&SET

        public Ofrecimientos Ofrecimientos
        {
            get => _ofrecimientos;
            set => SetPropertyValue(nameof(Ofrecimientos), ref _ofrecimientos, value);
        }
        public Boolean Previs
        {
            get => _Previs;
            set => SetPropertyValue(nameof(Previs), ref _Previs, value);
        }
        [Description("Unidad móvil")]
        public Boolean UnidadMovil
        {
            get => _UnidadMovil;
            set => SetPropertyValue(nameof(UnidadMovil), ref _UnidadMovil, value);
        }
        public bool Impreso
        {
            get { return _Impreso; }
            set { _Impreso = value; }
        }
        [DevExpress.Xpo.DisplayName("Fecha entrega cita")]
        public DateTime FechaEntrega
        {
            get => _FechaEntrega;
            set => SetPropertyValue(nameof(FechaEntrega), ref _FechaEntrega, value);
        }
        [DevExpress.Xpo.DisplayName("Finalidad Cita")]
        public TipoReconocimiento TipoReconocimiento
        {
            get => _TipoReconocimiento;
            set => SetPropertyValue(nameof(TipoReconocimiento), ref _TipoReconocimiento, value);
        }
        public DateTime FechaCita
        {
            get
            {

                //if (IsLoading != true)
                //{
                //   if (_FechaEntrega = new DateTime();
                //}
                return _FechaCita;
            }
            set
            {
                SetPropertyValue(nameof(FechaCita), ref _FechaCita, value);

            }
        }

        private TimeSpan _Hora;

        public TimeSpan Hora
        {
            get { return _Hora; }
            set { _Hora = value; }
        }
        public bool NoPresentado
        {
            get
            {


                return _NoPresentado;

            }
            set { _NoPresentado = value; }
        }
        public bool CitaAnulada
        {
            get
            {

                if (IsLoading != true)
                {
                    if (_CitaAnulada) _NoPresentado = _CitaAnulada;
                }
                return _CitaAnulada;
            }
            set { _CitaAnulada = value; }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }
        public Guid Evento
        {
            get => _Evento;
            set => SetPropertyValue(nameof(Evento), ref _Evento, value);
        }
        public string Subject
        {
            get
            {
                if (!IsLoading)
                {
                    var nombrecompleto = nombreCompleto() + " {Citas}";
                    return nombrecompleto;
                }
                return "{Citas}";
            }
            set
            {

                SetPropertyValue("Subject", ref _Subject, value);


            }
        }

        [Browsable(false)]
        private DateTime _FechaFin;

        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }


        private string nombreCompleto()
        {
            if (Empleado == null) return string.Empty;
            return (string.IsNullOrEmpty(Empleado.NombreCompleto)) ? string.Empty : Empleado.NombreCompleto;
        }
        public string Description { get => _Descripcion; set => SetPropertyValue("Descripcion", ref _Descripcion, value); }


        public DateTime StartOn { get => _FechaCita.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes); set => SetPropertyValue(nameof(FechaCita), ref _FechaCita, value.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes)); }

        [Browsable(false)]
        public DateTime EndOn { get => _FechaFin; set => SetPropertyValue(nameof(FechaFin), ref _FechaFin, value); }

        [Browsable(false)]
        public bool AllDay { get => false; set => SetPropertyValue("AllDay", ref _AllDay, value); }
        public string Location { get => _Location; set => SetPropertyValue("Location", ref _Location, value); }
        public int Label { get => _Label; set => SetPropertyValue("Label", ref _Label, value); }
        public int Status { get => _Status; set => SetPropertyValue("status", ref _Status, value); }
        public int Type { get => _Type; set => SetPropertyValue("Type", ref _Type, value); }
        public string ResourceId { get => _ResourceId; set => SetPropertyValue("ResourceId", ref _ResourceId, value); }

        public object AppointmentId { get { return Oid; } }

        #endregion
        #region METODOS
        public Citas(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Label = 1;
        }
        protected override void OnDeleting()
        {
           

                XPCollection<Reconocimientos> citasaborrar = new XPCollection<Reconocimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Citas.Oid] =?", this.Oid));
                if ((citasaborrar.Count != 0) & (citasaborrar != null))
                {
                    if (MessageBox.Show("Este empleado tiene un reconocimiento. Lo borramos ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                    string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                    string strsql = "Delete Reconocimientos Where Citas=" + this.Oid.ToString();
                    using (SqlConnection con = new SqlConnection(contection))
                    {
                        //ofr.Session.ExecuteQuery(strsql);
                        con.Open();

                        //Iniciamos una transaccion local.
                        SqlTransaction sqlTran = con.BeginTransaction();

                        SqlCommand cmd = new SqlCommand(strsql, con);

                        cmd.Transaction = sqlTran;
                        cmd.CommandText = strsql;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        sqlTran.Commit();
                        con.Close();
                    }
                }
                }
            
          //  Session.CommitTransaction();
        }
        protected override void OnSaved()
        {
            if ((this.NoPresentado == true) & (CitaAnulada) & (this.Ofrecimientos!=null))
            {

                XPCollection<Ofrecimientos> NoPresentadoOfrecimientos = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("Oid=?", this.Ofrecimientos.Oid));

                foreach (Ofrecimientos ofre in NoPresentadoOfrecimientos)
                {
                    ofre.CitaRecibida = false;
                    ofre.SolicitudRealizada = false;
                    ofre.Automatico = true;
                    //this.Ofrecimientos = null;
                    ofre.Save();
                    string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                    string strsql = "Delete Solicitudes Where Ofrecimiento="+ofre.Oid.ToString();
                    using (SqlConnection con = new SqlConnection(contection))
                    {
                        //ofr.Session.ExecuteQuery(strsql);
                        con.Open();

                        //Iniciamos una transaccion local.
                        SqlTransaction sqlTran = con.BeginTransaction();

                        SqlCommand cmd = new SqlCommand(strsql, con);

                        cmd.Transaction = sqlTran;
                        cmd.CommandText = strsql;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        sqlTran.Commit();
                        con.Close();
                    }

                    Solicitudes solicitudes = new Solicitudes(this.Session);
                    solicitudes.CitaRecibida = false;
                    solicitudes.Description = ofre.Description;
                    solicitudes.Empleado = Empleado;
                    solicitudes.Fecha = DateTime.Now;
                    solicitudes.Obsevaciones = "No presentado";
                    solicitudes.Ofrecimiento = ofre;
                    solicitudes.Cita = null;
                    solicitudes.Save();

                };

                this.Session.CommitTransaction();
            }
            if ((this.CitaAnulada == true) & (!NoPresentado) & (this.Ofrecimientos != null))
            {

                XPCollection<Ofrecimientos> NoPresentadoOfrecimientos = new XPCollection<Ofrecimientos>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("Oid=?", this.Ofrecimientos.Oid));

                foreach (Ofrecimientos ofre in NoPresentadoOfrecimientos)
                {
                    ofre.CitaRecibida = false;
                    ofre.SolicitudRealizada = true;
                    ofre.Automatico = true;
                    //this.Ofrecimientos = null;
                    ofre.Save();
                    string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                    string strsql = "Delete Solicitudes Where Ofrecimientos=" + ofre.Oid.ToString();
                    using (SqlConnection con = new SqlConnection(contection))
                    {
                        //ofr.Session.ExecuteQuery(strsql);
                        con.Open();

                        //Iniciamos una transaccion local.
                        SqlTransaction sqlTran = con.BeginTransaction();

                        SqlCommand cmd = new SqlCommand(strsql, con);

                        cmd.Transaction = sqlTran;
                        cmd.CommandText = strsql;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        sqlTran.Commit();
                        con.Close();
                    }
                    Solicitudes solicitudes = new Solicitudes(this.Session);
                    solicitudes.CitaRecibida = false;
                    solicitudes.Description = ofre.Description;
                    solicitudes.Empleado = Empleado;
                    solicitudes.Fecha = DateTime.Now;
                    solicitudes.Obsevaciones = "Cita anulada";
                    solicitudes.Ofrecimiento = ofre;
                    solicitudes.Cita = null;
                    solicitudes.Save();
                };
                try
                {
                    this.Session.CommitTransaction();
                }
                catch { }
            }
        }
        public void OnCreated()
        {
        }
        
        void IXafEntityObject.OnLoaded()
        {

        }
        void IXafEntityObject.OnSaving()
        {
            DateTime dfecha = this.FechaCita;
            try
            {
                dfecha = dfecha.AddMonths(-3);

            }
            catch
            {
                dfecha = DateTime.Now.AddMonths(-3);
            }
            DateTime dfecha2 = this.FechaCita;
            try
            {
                dfecha2 = dfecha2.AddMonths(3);

            }
            catch
            {
                dfecha2 = DateTime.Now.AddMonths(3);
            }
            XPCollection<Citas> citasant = new XPCollection<Citas>(this.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Oid] <> ? And [Empleado] = ? And ([FechaCita] >= ? AND [FechaCita]<= ?) AND [CitaAnulada]=?", this.Oid, this.Empleado, dfecha,dfecha2, false));
            if (citasant.Count != 0)
            {
                if (MessageBox.Show("Este empleado ya tiene otra cita. Continuamos ?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information)== System.Windows.Forms.DialogResult.No)
                {
                    {
                        try
                        {
                            this.Delete();
                        }
                        catch
                        {

                        }
                        return;
                    }
                }
            }
            Event @event;
            if (this.Evento != Guid.Empty)
            {
                @event = this.Session.GetObjectByKey<Event>(this.Evento);
            }
            else
            {
                @event = new Event(this.Session);
            }
            if (@event != null)
            {
                @event.AllDay = this.AllDay;
                @event.Description = this.Description;
                @event.EndOn = this.FechaCita.Date;
                @event.EndOn = @event.EndOn.AddHours(Hora.Hours).AddMinutes(Hora.Minutes);
                @event.Label = this.Label;
                @event.Location = this.Location;
                @event.StartOn = EndOn;
                @event.Status = this.Status;
                @event.Subject = this.Subject;
                @event.Type = this.Type;
                @event.Save();
                this.Evento = @event.Oid;
                if (IsDeleted) @event.Delete();
                // this.Save();
            }
            
        }
        #endregion
    }
}