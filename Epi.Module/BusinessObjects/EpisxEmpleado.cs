﻿using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class EpisxEmpleado : XPObject, IXafEntityObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private bool _Entregado;
        private DateTime _FechaEntrega;
        private DateTime _FechaDevolución;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        #endregion
        #region Colecciones

        //[Association("EpisxEmpleado-Epis")]
        //public XPCollection<Epis> Epis
        //{
        //    get
        //    {
        //        return GetCollection<Epis>("Epis");
        //    }
        //}

        [Association("EpisxEmpleado-Epis")]
        public IList<EpisxEmpleadoEpis> EpisxEmpleadoEpis
        {
            get
            {
                return GetList<EpisxEmpleadoEpis>("EpisxEmpleadoEpis");
            }
        }


        #endregion
        #region GET&SET

        private Image _Firma;

        [Browsable(false)]
        public Image Firma
        {
            get { return _Firma; }
            set { _Firma = value; }
        }

        public bool Entregado
        {
            get => _Entregado;
            set => SetPropertyValue(nameof(Entregado), ref _Entregado, value);
        }
        public DateTime FechaEntrega
        {
            get => _FechaEntrega;
            set => SetPropertyValue(nameof(FechaEntrega), ref _FechaEntrega, value);
        }
        public DateTime FechaDevolución
        {
            get => _FechaDevolución;
            set
            {
                if (_FechaDevolución == value)
                {
                    return;
                }

                SetPropertyValue(nameof(FechaDevolución), ref _FechaDevolución, value);

                if (!IsLoading)
                {
                    foreach (var item in EpisxEmpleadoEpis)
                    {
                        item.FechaDevolucion = this.FechaDevolución;
                        item.Save();
                    }
                    this.Save();
                    this.Session.CommitTransaction();
                }
            }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }

        #endregion
        #region METODOS
        public EpisxEmpleado(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        public void OnCreated()
        {
        }

        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {

        }

        #endregion
    }
}