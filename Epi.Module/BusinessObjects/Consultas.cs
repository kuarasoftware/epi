﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("Ofrecimientos")]
    [Description("Vacunas/Pruebas")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Consultas : XPObject, IXafEntityObject, IEvent
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private DateTime _FechaEntrega;
        private DateTime _FechaCita;
        private Epi.Module.BusinessObjects.Temel.Employee _Empleado;
        private string _Observaciones;
        private string _Descripcion;
        private bool _NoPresentado;
        private bool _CitaAnulada;
        private TipoReconocimiento _TipoReconocimiento;
        private bool _AllDay;
        private int _Label;
        private int _Type;
        private string _ResourceId;
        private string _Subject;
        private string _Location;
        private int _Status;
        private Guid _Evento;
        #endregion


        #region GET&SET

        [DevExpress.Xpo.DisplayName("Fecha entrega cita")]
        [Description("Fecha entrega cita")]
        public DateTime FechaEntrega
        {
            get => _FechaEntrega;
            set => SetPropertyValue(nameof(FechaEntrega), ref _FechaEntrega, value);
        }
        [DevExpress.Xpo.DisplayName("Finalidad cita")]
        [Browsable(true)]
        public TipoReconocimiento TipoReconocimiento
        {
            get => _TipoReconocimiento;
            set => SetPropertyValue(nameof(TipoReconocimiento), ref _TipoReconocimiento, value);
        }
        [DevExpress.Xpo.DisplayName("Fecha prueba o vacuna")]
        [Description("Fecha prueba o vacuna")]
        public DateTime FechaCita
        {
            get
            {

                //if (IsLoading != true)
                //{
                //   if (_FechaEntrega = new DateTime();
                //}
                return _FechaCita;
            }
            set
            {
                SetPropertyValue(nameof(FechaCita), ref _FechaCita, value);

            }
        }

        private TimeSpan _Hora;

        public TimeSpan Hora
        {
            get { return _Hora; }
            set { _Hora = value; }
        }
        public bool NoPresentado
        {
            get
            {


                return _NoPresentado;

            }
            set { _NoPresentado = value; }
        }
        public bool CitaAnulada
        {
            get
            {

                if (IsLoading != true)
                {
                    if (_CitaAnulada) _NoPresentado = _CitaAnulada;
                }
                return _CitaAnulada;
            }
            set { _CitaAnulada = value; }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        public Epi.Module.BusinessObjects.Temel.Employee Empleado
        {
            get
            {

                return _Empleado;
            }

            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }
        public Guid Evento
        {
            get => _Evento;
            set => SetPropertyValue(nameof(Evento), ref _Evento, value);
        }
        public string Subject
        {
            get
            {
                if (!IsLoading)
                {
                    var nombrecompleto = nombreCompleto() + " {Vacunas}";
                    return nombrecompleto;
                }
                return "{Vacunasas}";
            }
            set
            {

                SetPropertyValue("Subject", ref _Subject, value);


            }
        }

        [Browsable(false)]
        private DateTime _FechaFin;

        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }


        private string nombreCompleto()
        {
            if (Empleado == null) return string.Empty;
            return (string.IsNullOrEmpty(Empleado.NombreCompleto)) ? string.Empty : Empleado.NombreCompleto;
        }
        [Browsable(false)]
        public string Description { get => _Descripcion; set => SetPropertyValue("Descripcion", ref _Descripcion, value); }


        public DateTime StartOn { get => _FechaCita.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes); set => SetPropertyValue(nameof(FechaCita), ref _FechaCita, value.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes)); }
        [Browsable(false)]
        public DateTime EndOn { get => _FechaFin; set => SetPropertyValue(nameof(FechaFin), ref _FechaFin, value); }

        [Browsable(false)]
        public bool AllDay { get => false; set => SetPropertyValue("AllDay", ref _AllDay, value); }
        public string Location { get => _Location; set => SetPropertyValue("Location", ref _Location, value); }
        public int Label { get => _Label; set => SetPropertyValue("Label", ref _Label, value); }
        public int Status { get => _Status; set => SetPropertyValue("status", ref _Status, value); }
        public int Type { get => _Type; set => SetPropertyValue("Type", ref _Type, value); }
        public string ResourceId { get => _ResourceId; set => SetPropertyValue("ResourceId", ref _ResourceId, value); }

        public object AppointmentId { get { return Oid; } }

        #endregion
        #region METODOS
        public Consultas(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Label = 4;
        }
        protected override void OnSaved()
        {
           
            
        }
        public void OnCreated()
        {
        }
        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {
            Event @event;
            if (this.Evento != Guid.Empty)
            {
                @event = this.Session.GetObjectByKey<Event>(this.Evento);
            }
            else
            {
                @event = new Event(this.Session);
            }
            if (@event != null)
            {
                @event.AllDay = this.AllDay;
                @event.Description = this.Description;
                @event.EndOn = this.FechaCita.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes);
                @event.Label = this.Label;
                @event.Location = this.Location;
                @event.StartOn = this.FechaCita.Date.AddHours(Hora.Hours).AddMinutes(Hora.Minutes);
                @event.Status = this.Status;
                @event.Subject = this.Subject;
                @event.Type = this.Type;
                @event.Save();
                this.Evento = @event.Oid;
                this.Save();
                if (IsDeleted) @event.Delete();
            }
        }
        #endregion
    }
}