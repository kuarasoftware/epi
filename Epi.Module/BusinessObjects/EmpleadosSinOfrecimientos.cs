﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DomainComponent]
    [DefaultClassOptions]
    public class EmpleadosSinOfrecimientos : IXafEntityObject, IObjectSpaceLink, INotifyPropertyChanged
    {
        private IObjectSpace objectSpace;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public EmpleadosSinOfrecimientos()
        {
        }
        [Browsable(false)]  // Hide the entity identifier from UI.
        public int ID { get; set; }

        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private BusinessObjects.Temel.Employee _Empleado;
        public BusinessObjects.Temel.Employee Empleado
        {
            get { return _Empleado; }
            set { _Empleado = value; }
        }
        private string _Puesto;

        public string Puesto
        {
            get { return _Puesto; }
            set { _Puesto = value; }
        }
        private DateTime _Fecha;
        [DevExpress.Xpo.DisplayName("Fecha último ofrecimiento")]
        public DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }
        private DateTime _ProximaFecha;
        [DevExpress.Xpo.DisplayName("Próximo ofrecimiento")]
        [Description("Próxima fecha")]
        public DateTime ProximaFecha
        {
            get { return _ProximaFecha; }
            set { _ProximaFecha = value; }
        }
        private int _Periodicidad;
        public int Periodicidad
        {
            get { return _Periodicidad; }
            set { _Periodicidad = value; }
        }
        private Reconocimientos _reconocimientos;
        public Reconocimientos reconocimientos
        {
            get { return _reconocimientos; }
            set { _reconocimientos = value; }
        }

        #region IXafEntityObject members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIXafEntityObjecttopic.aspx)
        void IXafEntityObject.OnCreated()
        {
            // Place the entity initialization code here.
            // You can initialize reference properties using Object Space methods; e.g.:
            // this.Address = objectSpace.CreateObject<Address>();
        }
        void IXafEntityObject.OnLoaded()
        {
            // Place the code that is executed each time the entity is loaded here.
        }
        void IXafEntityObject.OnSaving()
        {
            // Place the code that is executed each time the entity is saved here.
        }
        #endregion

        #region IObjectSpaceLink members (see https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppIObjectSpaceLinktopic.aspx)
        // Use the Object Space to access other entities from IXafEntityObject methods (see https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113707.aspx).
        IObjectSpace IObjectSpaceLink.ObjectSpace
        {
            get { return objectSpace; }
            set { objectSpace = value; }
        }
        #endregion

        #region INotifyPropertyChanged members (see http://msdn.microsoft.com/en-us/library/system.componentmodel.inotifypropertychanged(v=vs.110).aspx)
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}