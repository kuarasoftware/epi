﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Linq;

namespace Epi.Module.BusinessObjects
{
    [DeferredDeletion(false)]
    public class EpisxEmpleadoEpis : BaseObject
    {
        public EpisxEmpleadoEpis(Session session) : base(session) { }

        private EpisxEmpleado episxEmpleado;
        [Association("EpisxEmpleado-Epis")]
        public EpisxEmpleado EpisxEmpleado
        {
            get { return episxEmpleado; }
            set { SetPropertyValue("EpisxEmpleado", ref episxEmpleado, value); }
        }

        private Epis epis;
        [Association("Epis-EpisxEmpleado")]
        public Epis Epis
        {
            get { return epis; }
            set { SetPropertyValue("Epis", ref epis, value); }
        }
        private int _Cantidad;

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
        private DateTime _FechaDevolucion;

        public DateTime FechaDevolucion
        {
            get { return _FechaDevolucion; }
            set
            {
                if (_FechaDevolucion == value)
                {
                    return;
                }

                _FechaDevolucion = value;

                if ((!IsLoading) && (Estado != TextOnlyEnum.Disposicion))
                {
                    this.Estado = TextOnlyEnum.Devuelto;
                    this.Save();
                    this.Session.CommitTransaction();
                }
            }
        }

        private string _Observaciones;

        [Size(SizeAttribute.Unlimited)]
        public string Observaciones
        {
            get { return _Observaciones; }
            set { _Observaciones = value; }
        }

        private TextOnlyEnum _Estado;

        public TextOnlyEnum Estado
        {
            get { return _Estado; }
            set { SetPropertyValue("Estado", ref _Estado, value); }
        }

        public enum TextOnlyEnum { Entregado, Disposicion, Devuelto, Específico, Otros };

    }
}
