﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Win.Templates;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ToolbarViewController : ViewController
    {
        // Separar por ';' los ListView que vayamos a ocultar
        string IdListViewAOcultar =
            "Epis_ListView;" +
            "EpisxEmpleado_ListView;" +
            "EpisxEmpleadoEpis.cs_ListView;";

        protected override void OnActivated()
        {
            base.OnActivated();

            List<string> ListViewsAOcultar = IdListViewAOcultar.Split(';').ToList();

            if (ListViewsAOcultar.Any(str => str == View.Id))
            {
                try
                {
                    Frame.GetController<DevExpress.ExpressApp.Win.SystemModule.ToolbarVisibilityController>().ShowToolbarAction.Active["Never"] = false;
                    Frame.TemplateChanged += Frame_TemplateChanged;
                }
                catch (Exception ex)
                {
                    //Estamos en ruta web y no hay frame
                }
            }
        }
        private NestedFrameTemplate NestedFrameTemplate
        {
            get { return Frame.Template as NestedFrameTemplate; }
        }
        void Frame_TemplateChanged(object sender, EventArgs e)
        {
            if (NestedFrameTemplate != null)
            {
                NestedFrameTemplate.ToolBar.Visible = false;
                NestedFrameTemplate.ToolBar.VisibleChanged += new EventHandler(ToolBar_VisibleChanged);
            }
        }
        void ToolBar_VisibleChanged(object sender, EventArgs e)
        {
            if (NestedFrameTemplate.ToolBar.Visible)
                NestedFrameTemplate.ToolBar.Visible = false;
        }
    }
}
