﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;
using DevExpress.XtraReports.UI;
using Epi.Module.BusinessObjects;
using Epi.Module.Repos;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BotonesCitas : ViewController
    {
        public BotonesCitas()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        private void VerCita_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            if (View.CurrentObject == null)
            {
                MessageBox.Show("Seleccione una fila", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            IObjectSpace objectSpace;
            Citas cita;
            //"Solicitudes_ListView;Solicitudes_DatailView;Ofrecimientos_ListView_Listview;Ofrecimientos_DetailView;EmpleadosSinReconocimientos_ListView;EmpleadosVerifica_ListView"
            switch (View.Id)
            {
                case "Solicitudes_ListView":
                case "Solicitudes_DatailView":
                    Solicitudes sol = (Solicitudes)View.CurrentObject;
                    if (sol.Cita==null)
                    {
                        MessageBox.Show("No hay ninguna cita para esta solicitud", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = sol.Cita;
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView = this.Application.CreateDetailView(objectSpace, cita,false);
                    detailView.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView;
                    break;
                case "Ofrecimientos_ListView_Listview":
                case "Ofrecimientos_ListView":
                case "Ofrecimientos_DetailView":
                    Ofrecimientos ofr = (Ofrecimientos)View.CurrentObject;
                    XPCollection<Citas> citas = new XPCollection<Citas>(ofr.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimientos]=?", ofr.Oid));
                    if ((citas == null)||(citas.Count==0))
                    {
                        MessageBox.Show("No hay ninguna cita para este ofrecimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = citas[0];
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView1 = this.Application.CreateDetailView(View.ObjectSpace, cita,false);
                    detailView1.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.View;
                    e.ShowViewParameters.CreatedView = detailView1;
                    break;
                case "EmpleadosSinReconocimientos_ListView":
                    EmpleadosSinReconocimientos sreco = (EmpleadosSinReconocimientos)View.CurrentObject;
                    if (sreco.Cita == null)
                    {
                        MessageBox.Show("No hay ninguna cita para este Empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = sreco.Cita;
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView6 = this.Application.CreateDetailView(View.ObjectSpace, cita, false);
                    detailView6.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView6;
                    break;
                case "EmpleadosVerifica_ListView":
                    EmpleadosVerifica ver = (EmpleadosVerifica)View.CurrentObject;
                    if (ver.ultimacita == null)
                    {
                        MessageBox.Show("No hay ninguna cita para este empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = ver.ultimacita;
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView2 = this.Application.CreateDetailView(View.ObjectSpace, cita,false);
                    detailView2.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView2;
                    break;
                case "EmpleadosConCitadeBaja_ListView":
                    EmpleadosConCitadeBaja baja = (EmpleadosConCitadeBaja)View.CurrentObject;
                    if (baja.Cita == null)
                    {
                        MessageBox.Show("No hay ninguna cita para esta línea", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = baja.Cita;
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView3 = this.Application.CreateDetailView(View.ObjectSpace, cita, false);
                    detailView3.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView3;
                    break;
                case "Reconocimientos_ListView_ListView":
                case "Reconocimientos_ListView":
                case "Reconocimientos_DetailView":
                    Reconocimientos reco = (Reconocimientos)View.CurrentObject;
                    if (reco.Citas == null)
                    {
                        MessageBox.Show("No hay ninguna cita para este Reconocimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    cita = reco.Citas;
                    objectSpace = Application.CreateObjectSpace(typeof(Citas));
                    DetailView detailView4 = this.Application.CreateDetailView(View.ObjectSpace, cita, false);
                    detailView4.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView4;
                    break;
            }
        }
        private void CrearInformeCita_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Stream myStream;
                //SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //saveFileDialog1.Filter = "Archivo PDF |*.pdf";
                //saveFileDialog1.FilterIndex = 0;
                //saveFileDialog1.RestoreDirectory = true;

                if (true) //(saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (true) //((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        //myStream.Close();
                        CitasV2 se = new CitasV2((Citas)View.CurrentObject);
                        se.CreateDocument();
                        se.BringToFront();
                        se.ShowPreview();
                        //se.ExportToPdf((myStream as FileStream).Name);
                        //MessageBox.Show("Se ha generado el informe correctamente", "Información", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        //System.Diagnostics.Process.Start((myStream as FileStream).Name);
                    }
                   ((Citas)View.CurrentObject).Impreso = true;
                    ((Citas)View.CurrentObject).Save();
                }
                View.ObjectSpace.CommitChanges();
                View.Refresh();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error al crear o abrir el archivo, compruebe los datos introducidos.", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
        }
    }
}
