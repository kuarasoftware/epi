﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CancelarCierre : ViewController
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            if (this.View.Id == "Ofrecimientos_DetailView")
            {
                View.QueryCanClose += View_QueryCanCloseOfrecimiento;
            }
            else if (this.View.Id == "Reconocimientos_DetailView")
            {
                View.QueryCanClose += View_QueryCanCloseReconocimientos;
            }
            // Perform various tasks depending on the target View.
        }
        private void View_QueryCanCloseOfrecimiento(object sender, CancelEventArgs e)
        {
            string error = string.Empty; //string que servirá para dar el mensaje de error

            try
            {
                if (((Ofrecimientos)View.CurrentObject).IsLoading)
                {
                    return;
                }
            }
            catch
            {
                e.Cancel = false;
                return;
            }
            if (((Ofrecimientos)View.CurrentObject).Oid == -1)
            {
                return;
            }
            //Miramos que para cada objeto se cumplan las condiciones
            if (((Ofrecimientos)View.CurrentObject).Periodicidad <= 0 && ((Ofrecimientos)View.CurrentObject).Aceptado != true)
            {
                error += "Si se ha aceptado el ofrecimiento la periodicidad ha de ser mayor de 0.\n";
                e.Cancel = true;
            }
            if(((Ofrecimientos)View.CurrentObject).Automatico != true && ((Ofrecimientos)View.CurrentObject).Aceptado != true && ((Ofrecimientos)View.CurrentObject).NoAceptado != true)
            {
                error += "El ofrecimiento no esta aceptado no no aceptado.\n";
                e.Cancel = true;

            }
            if (error != string.Empty)
            {
                System.Windows.Forms.MessageBox.Show(error, "ATENCIÓN!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
            }
        }

        private void View_QueryCanCloseReconocimientos(object sender, CancelEventArgs e)
        {
            string error = string.Empty; //string que servirá para dar el mensaje de error

            try
            {
                if (((Reconocimientos)View.CurrentObject).IsLoading)
                {
                    return;
                }
            }
            catch
            {
                e.Cancel = false;
                return;
            }
            if (((Reconocimientos)View.CurrentObject).Oid == -1)
            {
                return;
            }
            //Miramos que para cada objeto se cumplan las condiciones
            if (((Reconocimientos)View.CurrentObject).Periodicidad <= 0)
            {
                error += "Es obligatorio que la periodicidad sea mayor de 0.\n";
                e.Cancel = true;
            }
            if (error != string.Empty)
            {
                System.Windows.Forms.MessageBox.Show(error, "ATENCIÓN!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
