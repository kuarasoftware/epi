﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using Epi.Module.Utils;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Filtros : ViewController //ObjectViewController<ListView, RUTA_XAF_XPO.Module.BusinessObjects.Contratacion.Contrato>
    {
        public Filtros()
        {
            InitializeComponent();

            RegisterActions(components);

            TargetViewId = string.Empty;

            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();

            ParametrosGlobales.ViewName = View.Id;

            if (View is DevExpress.ExpressApp.ListView)
            {
                //   View.ControlsCreated += new EventHandler(View_ControlsCreated);

                switch (View.Id)
                {

                    case "Documento_ListView":
                        ((DevExpress.ExpressApp.ListView)View).CollectionSource.Criteria[string.Empty] = CriteriaOperator.Parse("(OidTabla == ? AND Tabla == ?)", Epi.Module.Controllers.BotonesDocumentos.OIDDOCUMENTO, Epi.Module.Controllers.BotonesDocumentos.TABLADOCUMENTO);
                        break;
                }

            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();

        }

        private void Filtros_Activated(object sender, EventArgs e)
        {
            //if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(RUTA_XAF_XPO.Module.BusinessObjects.Contratacion.Contrato)) & View.Id == "Contrato_ListView")
            //{
            //    //CriteriaOperator criteria = CriteriaOperator.Parse("Hotel = 1 AND Identificador = 3");
            //    ((ListView)View).CollectionSource.Criteria["Filter1"] = criteria;
            //    //((ListView)View).CollectionSource.Criteria["Filter2"] = new BinaryOperator(
            //    //    "Identificador", 10, BinaryOperatorType.Less);

            //    //VariablesGlobales.strCadenaDeConexion
            //}
        }
    }
}
