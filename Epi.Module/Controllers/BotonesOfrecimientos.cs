﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;
using DevExpress.XtraReports.UI;
using Epi.Module.BusinessObjects;
using Epi.Module.Repos;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BotonesOfrecimientos : ViewController
    {
        public BotonesOfrecimientos()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CrearInformeOfrecimientos_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Stream myStream;
                //SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //saveFileDialog1.Filter = "Archivo PDF |*.pdf";
                //saveFileDialog1.FilterIndex = 0;
                //saveFileDialog1.RestoreDirectory = true;

                if (true) //(saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (true) //((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        //myStream.Close();
                        Ofrecimientos OfreP= (Ofrecimientos)View.CurrentObject;
                        Ofrecimiento ses= new Ofrecimiento(OfreP);
                        //XPCollection<Ofrecimientos> se=XPCollection<Ofrecimientos>(View.ObjectSpace.)
                        ses.CreateDocument();
                        int a = View.SelectedObjects.Count;
                        int b = 0;
                        foreach (Ofrecimientos Ofre in View.SelectedObjects)
                        {
                            b += 1;
                            if (Ofre.Oid != OfreP.Oid)
                            {
                                Ofrecimiento se = new Ofrecimiento(Ofre);
                            
                                se.CreateDocument();
                                if (!Ofre.Impreso) Ofre.Fecha = DateTime.Now;
                                if (!Ofre.Impreso) Ofre.FechaFin = DateTime.Now;
                                Ofre.Impreso = true;
                                //se.ShowPreviewDialog(false);
                                ses.Pages.AddRange(se.Pages);
                                
                            }
                            ses.ShowPreview();
                            View.ObjectSpace.CommitChanges();
                            //ReportPrintTool printTool = new ReportPrintTool(ses);
                            //printTool.ShowPreviewDialog();
                        }
                        
                        //se.ExportToPdf((myStream as FileStream).Name);
                        //MessageBox.Show("Se ha generado el informe correctamente", "Información", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        //System.Diagnostics.Process.Start((myStream as FileStream).Name);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error al crear o abrir el archivo, compruebe los datos introducidos.", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
        }

        private void CrearInformeSolicitud_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            SimpleAction sim = (SimpleAction)sender;
            IObjectSpace os = sim.Application.CreateObjectSpace();
            Solicitudes pofrecimientos = (Solicitudes)os.CreateObject<Solicitudes>();
            Boolean faltan = false;
            foreach (EmpleadosConCitadeBaja citas in View.SelectedObjects)
            {
                if ((citas.Cita != null) & (citas.Cita.Ofrecimientos != null))
                {
                    Ofrecimientos ofrecimientos = citas.Cita.Ofrecimientos;
                    Solicitudes solicitudes = new Solicitudes(pofrecimientos.Session);
                    solicitudes.CitaRecibida = false;
                    solicitudes.Description = ofrecimientos.Description;
                    solicitudes.Empleado = (BusinessObjects.Temel.Employee)os.GetObjectByKey<BusinessObjects.Temel.Employee>(ofrecimientos.Empleado.No_);
                    solicitudes.Fecha = DateTime.Now;
                    solicitudes.Obsevaciones = ofrecimientos.Obsevaciones;
                    solicitudes.Ofrecimiento = (Ofrecimientos)os.GetObjectByKey<Ofrecimientos>(ofrecimientos.Oid);
                    solicitudes.Save();
                }
                else
                {
                    faltan = true;
                }
            }
                        os.CommitChanges();
            if (faltan) MessageBox.Show("Algunas de las citas no tienen ofrecimientos y, por tanto no se ha podidi crear la solititud. Revise las citas", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            
        }
        private void OfrecimientosnoCreados_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            SimpleAction sim = (SimpleAction)sender;
            IObjectSpace objectSpace = sim.Application.CreateObjectSpace();
            Ofrecimientos pofrecimientos = (Ofrecimientos)objectSpace.CreateObject<Ofrecimientos>();
            foreach (Reconocimientos sinofrecimientos in View.SelectedObjects)
            {
                if (sinofrecimientos.Periodicidad >= 0)
                {
                    DateTime fecha = sinofrecimientos.Fecha;

                    bool pAceptado = false;
                    if ((sinofrecimientos.Calificacion.Oid == 4) || (sinofrecimientos.Calificacion.Oid == 5)) pAceptado = true;
                    //if (pAceptado)
                    fecha = fecha.AddMonths(sinofrecimientos.Periodicidad);
                    //if (! pAceptado) fecha =sinofrecimientos.Citas.Ofrecimientos.Fecha.AddMonths(Periodicidad);
                    if (fecha.DayOfWeek == DayOfWeek.Saturday)
                    {
                        fecha = fecha.AddDays(2);
                    }
                    else if (fecha.DayOfWeek == DayOfWeek.Sunday)
                    {
                        fecha = fecha.AddDays(1);
                    }
                    if (pAceptado)
                    {
                        XPCollection<Ofrecimientos> ofrecimientosant = new XPCollection<Ofrecimientos>(sinofrecimientos.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Empleado] = ? And ([Fecha] >= ? AND [Fecha]<= ?)", sinofrecimientos.Empleado, fecha.AddMonths(-3), fecha.AddMonths(3)));
                        if ((ofrecimientosant.Count == 0) || (ofrecimientosant == null))
                        {
                            Ofrecimientos ofrecimiento = new Ofrecimientos(sinofrecimientos.Session)
                            {
                                Description = sinofrecimientos.Description,
                                Obsevaciones = sinofrecimientos.Obsevaciones,
                                Fecha = fecha,
                                Empleado = sinofrecimientos.Empleado,
                                Periodicidad = sinofrecimientos.Periodicidad,
                                TipoOfrecimiento = sinofrecimientos.Session.GetObjectByKey<BusinessObjects.TipoOfrecimiento>(1),
                                Aceptado = pAceptado,
                                Periodico = true,
                                Automatico=true
                            };
                            ofrecimiento.Save();
                        }
                    }
                    else
                    {
                        XPCollection<Ofrecimientos> ofrecimientosant = new XPCollection<Ofrecimientos>(sinofrecimientos.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Empleado] = ? And ([Fecha] >= ? AND [Fecha]<= ?)", sinofrecimientos.Empleado, fecha.AddMonths(-3),fecha.AddMonths(3)));
                        if ((ofrecimientosant.Count == 0) || (ofrecimientosant == null))
                        {
                            Ofrecimientos ofrecimiento = new Ofrecimientos(sinofrecimientos.Session)
                            {

                                Description = sinofrecimientos.Description,
                                Obsevaciones = sinofrecimientos.Obsevaciones,
                                Fecha = fecha,
                                Empleado = sinofrecimientos.Empleado,
                                Periodicidad = sinofrecimientos.Periodicidad,
                                TipoOfrecimiento = sinofrecimientos.Session.GetObjectByKey<BusinessObjects.TipoOfrecimiento>(1),
                                Periodico = true,
                                Automatico=true
                            };
                            ofrecimiento.Save();
                        }
                    }
                }
            }
        }
        private void CrearOfrecimientos_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            //  IObjectSpace OS=Emp.Empleado.Session.o
            SimpleAction sim = (SimpleAction)sender;
             IObjectSpace objectSpace = sim.Application.CreateObjectSpace();
            //Ofrecimientos pofrecimientos = (Ofrecimientos)objectSpace.CreateObject<Ofrecimientos>();
            
            int a = View.SelectedObjects.Count;
            foreach (EmpleadosSinOfrecimientos sinofrecimientos in View.SelectedObjects)
            {
                DateTime fecha = sinofrecimientos.ProximaFecha;
                if (fecha < DateTime.Now) fecha = DateTime.Now;
                XPCollection<Ofrecimientos> ofrecimientosant = new XPCollection<Ofrecimientos>(sinofrecimientos.Empleado.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Empleado] = ? And ([Aceptado] = ? AND [NoAceptado]= ?)", sinofrecimientos.Empleado, 0, 0));
                for (int i = 0; i < ofrecimientosant.Count; i++)
                {
                Ofrecimientos ofreaborrar= objectSpace.GetObject<Ofrecimientos>(ofrecimientosant[i]);
                    ofreaborrar.Delete();
                    //sinofrecimientos.Empleado.Session.Delete(ofrecimientosant);

                }
                Ofrecimientos ofrecimientos = objectSpace.CreateObject<Ofrecimientos>();// new Ofrecimientos(sinofrecimientos.Empleado.Session)

                ofrecimientos.Aceptado = false;
                ofrecimientos.CitaRecibida = false;
                ofrecimientos.Description = "Cita médica";
                ofrecimientos.Empleado =ofrecimientos.Session.GetObjectByKey<BusinessObjects.Temel.Employee>(sinofrecimientos.Empleado.No_);
                ofrecimientos.Fecha = fecha;
                    ofrecimientos.FechaFin = fecha;
                ofrecimientos.NoAceptado = false;
                    ofrecimientos.Obsevaciones = "";
                    ofrecimientos.Periodicidad = sinofrecimientos.Periodicidad;
                ofrecimientos.SolicitudRealizada = false;
                ofrecimientos.TipoOfrecimiento = (TipoOfrecimiento)ofrecimientos.Session.GetObjectByKey<TipoOfrecimiento>(2);
                ofrecimientos.Automatico = true;
                ofrecimientos.Save();
                  // .CommitChanges();
            }
            //View.ObjectSpace.CommitChanges();
            objectSpace.CommitChanges();
            MessageBox.Show("Se han creado " + a.ToString() + " ofrecimientos, a partir de los empleados sin ofrecimiento seleccionados");
            View.RefreshDataSource();
            View.Refresh();
            View.ObjectSpace.Refresh();
            
        }
        private void CrearInformeOfrecimiento_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Stream myStream;
                //SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //saveFileDialog1.Filter = "Archivo PDF |*.pdf";
                //saveFileDialog1.FilterIndex = 0;
                //saveFileDialog1.RestoreDirectory = true;

                if (true) //(saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (true) //((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        //myStream.Close();
                        Ofrecimiento se = new Ofrecimiento((Ofrecimientos)View.CurrentObject);
                        se.CreateDocument();
                        //se.ExportToPdf((myStream as FileStream).Name);
                        //MessageBox.Show("Se ha generado el informe correctamente", "Información", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        //System.Diagnostics.Process.Start((myStream as FileStream).Name);
                        se.ShowPreview();
                        ((Ofrecimientos)View.CurrentObject).Impreso = true;
                        ((Ofrecimientos)View.CurrentObject).Automatico = true;
                        ((Ofrecimientos)View.CurrentObject).Save();
                    }
                    View.ObjectSpace.CommitChanges();
                    View.Refresh();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error al crear o abrir el archivo, compruebe los datos introducidos.", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
        }
        private void VerOfrecimiento_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            if (View.CurrentObject == null)
            {
                MessageBox.Show("Seleccione una fila", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            IObjectSpace objectSpace;
            Ofrecimientos ofrecimientos;
            
            switch (View.Id)
            {
                case "Solicitudes_ListView":
                case "Solicitudes_DatailView":
                    Solicitudes sol = (Solicitudes)View.CurrentObject;
                    if (sol.Ofrecimiento == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para esta solicitud", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = sol.Ofrecimiento;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView = this.Application.CreateDetailView(objectSpace,objectSpace.GetObjectByKey<Ofrecimientos>(sol.Ofrecimiento.Oid),false);
                    detailView.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView;
                    break;
                
                case "Citas_ListView_ListView":
                case "Citas_ListView":
                case "Citas_DetailView":
                    Citas cita = (Citas)View.CurrentObject;
                    if (cita.Ofrecimientos == null) 
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para esta cita", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = cita.Ofrecimientos;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView1 = this.Application.CreateDetailView(View.ObjectSpace, ofrecimientos, false);
                    detailView1.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.View;
                    e.ShowViewParameters.CreatedView = detailView1;
                    break;
                case "EmpleadosSinReconocimientos_ListView":
                    EmpleadosSinReconocimientos sreco = (EmpleadosSinReconocimientos)View.CurrentObject;
                    if (sreco.Cita == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para este Empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    if (sreco.Cita.Ofrecimientos == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para este Empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = sreco.Cita.Ofrecimientos;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView6 = this.Application.CreateDetailView(View.ObjectSpace, ofrecimientos, false);
                    detailView6.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView6;
                    break;
                case "EmpleadosVerifica_ListView":
                    EmpleadosVerifica ver = (EmpleadosVerifica)View.CurrentObject;
                    if (ver.ultimoofrecimiento == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para este empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = ver.proximoofrecimiento;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView2 = this.Application.CreateDetailView(View.ObjectSpace, ofrecimientos, false);
                    detailView2.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView2;
                    break;
                case "EmpleadosConCitadeBaja_ListView":
                    EmpleadosConCitadeBaja baja = (EmpleadosConCitadeBaja)View.CurrentObject;
                    if (baja.Cita == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para esta línea", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    if (baja.Cita.Ofrecimientos == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimiento para esta línea", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = baja.Cita.Ofrecimientos;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView3 = this.Application.CreateDetailView(View.ObjectSpace, ofrecimientos, false);
                    detailView3.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView3;
                    break;

                case "Reconocimientos_ListView_Listview":
                case "Reconocimientos_ListView":
                case "Reconocimientos_DetailView":
                    Reconocimientos reco = (Reconocimientos)View.CurrentObject;
                    if (reco.Citas == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimientlo para este Reconocimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    if (reco.Citas.Ofrecimientos == null)
                    {
                        MessageBox.Show("No hay ningun ofrecimientlo para este Reconocimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    ofrecimientos = reco.Citas.Ofrecimientos;
                    objectSpace = Application.CreateObjectSpace(typeof(Ofrecimientos));
                    DetailView detailView4 = this.Application.CreateDetailView(View.ObjectSpace, ofrecimientos, false);
                    detailView4.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView4;
                    break;
            }
        }

        private void ImprimirInformeSolicitud_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if(View != null && View.SelectedObjects.Count != 0)
            {
                SimpleAction sim = (SimpleAction)sender;
                //IObjectSpace objectSpace = sim.Application.CreateObjectSpace();
                Solicitud rptSolicitudes = new Solicitud(this.View);

                rptSolicitudes.ShowPreview();
                foreach (Solicitudes sol in View.SelectedObjects)
                {
                    sol.Comunicadoalamutua = true;
                    sol.Save();
                }
                View.ObjectSpace.CommitChanges();
            }
            else
            {
                this.Application.ShowViewStrategy.ShowMessage("Debe seleccionar almenos una solicitud.");
            }
        }
        private void Marcaradaptacion_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            string contection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            string strsql = "Update CHSD$Employee SET Adaptación=CASE when Adaptación=1 Then 0 else 1 end";
            switch (View.Id)
            {
                case "Ofrecimientos_ListView_Listview":
                case "Ofrecimientos_ListView":
                case "Ofrecimientos_DetailView":
                    Ofrecimientos ofr = (Ofrecimientos)View.CurrentObject;
                    if (ofr!=null)
                    {
                        if (ofr.Empleado!=null)
                        {
            
                            strsql += " Where No_='" + ofr.Empleado.No_ + "'";
                            using (SqlConnection con = new SqlConnection(contection))
                            {
                                //ofr.Session.ExecuteQuery(strsql);
                                con.Open();

                                //Iniciamos una transaccion local.
                                SqlTransaction sqlTran = con.BeginTransaction();

                                SqlCommand cmd = new SqlCommand(strsql, con);

                                cmd.Transaction = sqlTran;
                                cmd.CommandText = strsql;
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                sqlTran.Commit();
                                con.Close();
                            }
                        }
                        Ofrecimientos Ofre2 = View.ObjectSpace.GetObjectByKey<Ofrecimientos>(ofr.Oid);
                        Ofre2.Empleado.Reload();
                        View.CurrentObject = Ofre2;
                        View.Refresh(true);
                    }
                   
                    break;
                case "Reconocimientos_ListView_ListView":
                case "Reconocimientos_ListView":
                case "Reconocimientos_DetailView":
                    Reconocimientos reco = (Reconocimientos)View.CurrentObject;
                    if (reco != null)
                    {
                        if (reco.Empleado != null)
                        {
                            strsql += " Where No_='" + reco.Empleado.No_ + "'";
                            
                            using (SqlConnection con = new SqlConnection(contection))
                            {
                                //ofr.Session.ExecuteQuery(strsql);
                                con.Open();

                                //Iniciamos una transaccion local.
                                SqlTransaction sqlTran = con.BeginTransaction();

                                SqlCommand cmd = new SqlCommand(strsql, con);

                                cmd.Transaction = sqlTran;
                                cmd.CommandText = strsql;
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                                sqlTran.Commit();
                                con.Close();
                            }
                        }
                        Reconocimientos Reco2 = View.ObjectSpace.GetObjectByKey<Reconocimientos>(reco.Oid);
                        Reco2.Empleado.Reload();
                        View.CurrentObject = Reco2;
                        View.Refresh(true);
                    }
                   
                    break;
            }
            
            
        }
    }
}
