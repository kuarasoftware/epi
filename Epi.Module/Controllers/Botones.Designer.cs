﻿namespace Epi.Module.Controllers
{
    partial class Botones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ImportarEpis = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ImprimirSolicitud = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ImprimirDevolucion = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ImportarEpis
            // 
            this.ImportarEpis.Caption = "Importar Epis";
            this.ImportarEpis.ConfirmationMessage = null;
            this.ImportarEpis.Id = "ImportarEpis";
            this.ImportarEpis.TargetObjectType = typeof(Epi.Module.BusinessObjects.EpisxEmpleado);
            this.ImportarEpis.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ImportarEpis.ToolTip = null;
            this.ImportarEpis.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ImportarEpis.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ImportarEpis_Execute);
            // 
            // ImprimirSolicitud
            // 
            this.ImprimirSolicitud.Caption = "Imprimir Entrega";
            this.ImprimirSolicitud.ConfirmationMessage = null;
            this.ImprimirSolicitud.Id = "ImprimirSolicitud";
            this.ImprimirSolicitud.TargetObjectType = typeof(Epi.Module.BusinessObjects.EpisxEmpleado);
            this.ImprimirSolicitud.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ImprimirSolicitud.ToolTip = null;
            this.ImprimirSolicitud.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ImprimirSolicitud.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ImprimirSolicitud_Execute);
            // 
            // ImprimirDevolucion
            // 
            this.ImprimirDevolucion.Caption = "Imprimir Devolucion";
            this.ImprimirDevolucion.ConfirmationMessage = null;
            this.ImprimirDevolucion.Id = "ImprimirDevolucion";
            this.ImprimirDevolucion.TargetObjectType = typeof(Epi.Module.BusinessObjects.EpisxEmpleado);
            this.ImprimirDevolucion.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ImprimirDevolucion.ToolTip = null;
            this.ImprimirDevolucion.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ImprimirDevolucion.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ImprimirDevolucion_Execute);
            // 
            // Botones
            // 
            this.Actions.Add(this.ImportarEpis);
            this.Actions.Add(this.ImprimirSolicitud);
            this.Actions.Add(this.ImprimirDevolucion);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ImportarEpis;
        private DevExpress.ExpressApp.Actions.SimpleAction ImprimirSolicitud;
        private DevExpress.ExpressApp.Actions.SimpleAction ImprimirDevolucion;
    }
}
