﻿namespace Epi.Module.Controllers
{
    partial class BotonesOfrecimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BotonesOfrecimientos));
            this.CrearInformeOfrecimiento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearInformeOfrecimientos = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearInformeSolicitud = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ImprimirInformeSolicitud = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearOfrecimientos = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OfrecimientosnoCreados = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.VerOfrecimiento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.marcaradaptacion = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CrearInformeOfrecimiento
            // 
            this.CrearInformeOfrecimiento.Caption = "Crear Informe Ofrecimiento";
            this.CrearInformeOfrecimiento.ConfirmationMessage = null;
            this.CrearInformeOfrecimiento.Id = "CrearInformeOfrecimiento";
            this.CrearInformeOfrecimiento.TargetObjectType = typeof(Epi.Module.BusinessObjects.Ofrecimientos);
            this.CrearInformeOfrecimiento.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CrearInformeOfrecimiento.ToolTip = null;
            this.CrearInformeOfrecimiento.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CrearInformeOfrecimiento.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearInformeOfrecimiento_Execute);
            // 
            // CrearInformeOfrecimientos
            // 
            this.CrearInformeOfrecimientos.Caption = "Crear Informe Ofrecimientos";
            this.CrearInformeOfrecimientos.ConfirmationMessage = null;
            this.CrearInformeOfrecimientos.Id = "CrearInformeOfrecimientos";
            this.CrearInformeOfrecimientos.TargetObjectType = typeof(Epi.Module.BusinessObjects.Ofrecimientos);
            this.CrearInformeOfrecimientos.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CrearInformeOfrecimientos.ToolTip = null;
            this.CrearInformeOfrecimientos.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CrearInformeOfrecimientos.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearInformeOfrecimientos_Execute);
            // 
            // CrearInformeSolicitud
            // 
            this.CrearInformeSolicitud.Caption = "Crear Informe Solicitud";
            this.CrearInformeSolicitud.ConfirmationMessage = null;
            this.CrearInformeSolicitud.Id = "CrearInformeSolicitud";
            this.CrearInformeSolicitud.TargetObjectType = typeof(Epi.Module.BusinessObjects.EmpleadosConCitadeBaja);
            this.CrearInformeSolicitud.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CrearInformeSolicitud.ToolTip = null;
            this.CrearInformeSolicitud.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CrearInformeSolicitud.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearInformeSolicitud_Execute);
            // 
            // ImprimirInformeSolicitud
            // 
            this.ImprimirInformeSolicitud.Caption = "Imprimir Informe Solicitud";
            this.ImprimirInformeSolicitud.ConfirmationMessage = null;
            this.ImprimirInformeSolicitud.Id = "ImprimirInformeSolicitud";
            this.ImprimirInformeSolicitud.TargetObjectType = typeof(Epi.Module.BusinessObjects.Solicitudes);
            this.ImprimirInformeSolicitud.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.ImprimirInformeSolicitud.ToolTip = null;
            this.ImprimirInformeSolicitud.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.ImprimirInformeSolicitud.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ImprimirInformeSolicitud_Execute);
            // 
            // CrearOfrecimientos
            // 
            this.CrearOfrecimientos.Caption = "Crear Ofrecimientos";
            this.CrearOfrecimientos.ConfirmationMessage = null;
            this.CrearOfrecimientos.Id = "CrearOfrecimientos";
            this.CrearOfrecimientos.TargetObjectType = typeof(Epi.Module.BusinessObjects.EmpleadosSinOfrecimientos);
            this.CrearOfrecimientos.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CrearOfrecimientos.ToolTip = null;
            this.CrearOfrecimientos.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CrearOfrecimientos.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearOfrecimientos_Execute);
            // 
            // OfrecimientosnoCreados
            // 
            this.OfrecimientosnoCreados.Caption = "Crear Ofrecimientos de Reconocimeintos";
            this.OfrecimientosnoCreados.ConfirmationMessage = null;
            this.OfrecimientosnoCreados.Id = "CrearOfrecimientos2";
            this.OfrecimientosnoCreados.TargetObjectType = typeof(Epi.Module.BusinessObjects.Reconocimientos);
            this.OfrecimientosnoCreados.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.OfrecimientosnoCreados.ToolTip = null;
            this.OfrecimientosnoCreados.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.OfrecimientosnoCreados.Execute += OfrecimientosnoCreados_Execute;
            // 
            // VerOfrecimiento
            // 
            this.VerOfrecimiento.Caption = "Ver Ofrecimiento";
            this.VerOfrecimiento.ConfirmationMessage = null;
            this.VerOfrecimiento.Id = "VerOfrecimientos";
            this.VerOfrecimiento.TargetViewId = resources.GetString("VerOfrecimiento.TargetViewId");
            this.VerOfrecimiento.ToolTip = null;
            this.VerOfrecimiento.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.VerOfrecimiento.Execute += VerOfrecimiento_Execute;
            // 
            // marcaradaptacion
            // 
            this.marcaradaptacion.Caption = "Marcar/desmarcar Adaptación";
            this.marcaradaptacion.ConfirmationMessage = null;
            this.marcaradaptacion.Id = "Marcaradptacion";
            this.marcaradaptacion.ToolTip = null;
            this.marcaradaptacion.TargetViewId = "Reconocimientos_ListView;Reconocimientos_ListView_ListView;Reconocimientos_DetailView;EmpleadosConCitadeBaja_ListView";
            this.marcaradaptacion.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.marcaradaptacion.Execute += Marcaradaptacion_Execute;
            // 
            // BotonesOfrecimientos
            // 
            this.Actions.Add(this.CrearInformeOfrecimiento);
            this.Actions.Add(this.CrearInformeOfrecimientos);
            this.Actions.Add(this.CrearInformeSolicitud);
            this.Actions.Add(this.ImprimirInformeSolicitud);
            this.Actions.Add(this.CrearOfrecimientos);
            this.Actions.Add(this.OfrecimientosnoCreados);
            this.Actions.Add(this.VerOfrecimiento);
            this.Actions.Add(this.marcaradaptacion);

        }

       











        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CrearInformeOfrecimiento;
        private DevExpress.ExpressApp.Actions.SimpleAction CrearInformeOfrecimientos;
        private DevExpress.ExpressApp.Actions.SimpleAction CrearInformeSolicitud;
        private DevExpress.ExpressApp.Actions.SimpleAction ImprimirInformeSolicitud;
        private DevExpress.ExpressApp.Actions.SimpleAction CrearOfrecimientos;
        private DevExpress.ExpressApp.Actions.SimpleAction OfrecimientosnoCreados;
        private DevExpress.ExpressApp.Actions.SimpleAction VerOfrecimiento;
        private DevExpress.ExpressApp.Actions.SimpleAction marcaradaptacion;
    }
}
