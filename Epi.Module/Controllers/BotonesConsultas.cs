﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.XtraReports.UI;
using Epi.Module.BusinessObjects;
using Epi.Module.Repos;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BotonesConsultas : ViewController
    {
        public BotonesConsultas()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CrearInformeCita_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Stream myStream;
                //SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //saveFileDialog1.Filter = "Archivo PDF |*.pdf";
                //saveFileDialog1.FilterIndex = 0;
                //saveFileDialog1.RestoreDirectory = true;

                if (true) //(saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (true) //((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        //myStream.Close();
                        Consulta se = new Consulta((Consultas)View.CurrentObject);
                        se.CreateDocument();
                        se.BringToFront();
                        se.ShowPreview();
                        //se.ExportToPdf((myStream as FileStream).Name);
                        //MessageBox.Show("Se ha generado el informe correctamente", "Información", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        //System.Diagnostics.Process.Start((myStream as FileStream).Name);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error al crear o abrir el archivo, compruebe los datos introducidos.", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
        }
    }
}
