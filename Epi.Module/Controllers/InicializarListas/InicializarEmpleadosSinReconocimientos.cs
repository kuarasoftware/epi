﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public partial class InicializarEmpleadosSinReconocimientos : WindowController
    {
        public InicializarEmpleadosSinReconocimientos() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosSinReconocimientos)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosSinReconocimientos> objects = new BindingList<EmpleadosSinReconocimientos>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                objects.Add(new EmpleadosSinReconocimientos() {
                    Empleado =(BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString()),
                    Cita =(Citas)Os.GetObjectByKey<Citas>(drCurrent["OID"])
                });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();

            string sql = "SELECT DISTINCT CHSD$Employee.[No_],OID FROM Citas inner join CHSD$Employee on Citas.Empleado = CHSD$Employee.No_ Where (Select Count(OID) from Reconocimientos where Reconocimientos.Empleado = Citas.Empleado AND Reconocimientos.Fecha>=Citas.FechaCita) = 0 And IsNull(Citas.NoPresentado,0)= 0 AND ([Termination Date]>=GETDATE() OR [Termination Date]='1753-01-01 00:00:00.000')";
            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Empleados Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EmpleadosRestantes");
            daEpisRestantes.Fill(dsPubs, "EmpleadosRestantes");

            return dsPubs.Tables["EmpleadosRestantes"];
        }
    }
}
