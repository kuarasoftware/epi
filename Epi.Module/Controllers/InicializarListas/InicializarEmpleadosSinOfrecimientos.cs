﻿using DevExpress.ExpressApp;
using DevExpress.Xpo;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public class InicializarEmpleadosSinOfrecimiento : WindowController
    {
        IObjectSpace Os;
        public InicializarEmpleadosSinOfrecimiento() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosSinOfrecimientos)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                Os = (NonPersistentObjectSpace)e.CollectionSource.ObjectSpace;
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosSinOfrecimientos> objects = new BindingList<EmpleadosSinOfrecimientos>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                DateTime fecha = DateTime.MinValue;
                DateTime fecha2 = DateTime.Today.AddMonths(-12);
                DateTime fechareconocimiento= DateTime.MinValue;
                Reconocimientos Reco = null;
                try
                {
                    Reco = (BusinessObjects.Reconocimientos)Os.GetObjectByKey<BusinessObjects.Reconocimientos>(drCurrent["Reconocimiento"]);
                    fechareconocimiento = Reco.Fecha;
                }
                catch (Exception ex)
                { }
                try
                {

                    fecha = Convert.ToDateTime(drCurrent["Fecha"].ToString());
                    fecha2 = fecha;
                    if (fechareconocimiento == DateTime.MinValue) fechareconocimiento = fecha;
                }
                
                catch { }
                int periodicidad = Convert.ToInt32(drCurrent["Periodicidad"].ToString());// 12;
                fecha2 = fechareconocimiento;
                //try
                //{
                //    BusinessObjects.Temel.Employee Empleado = (BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString());
                //    Session session = new Session();
                //    session.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                //    Ofrecimientos ultimoofrecimiento = new XPCollection<Ofrecimientos>(session).ToList().OrderByDescending(x=> x.Fecha).Where(x=> x.Empleado!=null && (x.Empleado.No_ == Empleado.No_)).FirstOrDefault();
                //    periodicidad = ultimoofrecimiento.Periodicidad;
                //}
                //catch { }
                if (fecha2 != DateTime.MinValue) fecha2=fecha2.AddMonths(periodicidad);
                if (fecha2 < fecha) fecha2 = fecha;
                if (fecha < DateTime.Now)
                    {
                    objects.Add(new EmpleadosSinOfrecimientos() {
                        Empleado = (BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString()),
                        Nombre = drCurrent["Name"].ToString(),
                        Puesto = drCurrent["Statistics Group Code"].ToString(),
                        Fecha = fecha,
                        ProximaFecha = fecha2,
                        Periodicidad = periodicidad,
                    reconocimientos=Reco});
                }
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();
            string sql = "SELECT No_ ,[First Family Name]+' '+[Second Family Name]+','+ Name As Name, " +
"[Statistics Group Code]," +
"(Select Top 1 Fecha From Ofrecimientos where Ofrecimientos.Empleado = CHSD$Employee.No_ Order By Fecha Desc) AS Fecha" +
", isnull((Select Top 1 Periodicidad From Ofrecimientos where Ofrecimientos.Empleado = CHSD$Employee.No_ Order By Fecha Desc),12) AS Periodicidad, " +
"(Select Top 1 OID From Reconocimientos where Reconocimientos.Empleado = CHSD$Employee.No_ Order By Fecha Desc) AS Reconocimiento "
+" FROM CHSD$Employee where((CONVERT(varchar(16),[Termination Date],102)>=CONVERT(varchar(16), GETDATE(), 102) "
+ "OR [Termination Date] = '1753-01-01 00:00:00.000')) AND((ISNULL([Fecha Fin Excedencia],GETDATE()-1) < GETDATE())) "
+ "except Select Distinct No_ , [First Family Name]+' ' +[Second Family Name] + ',' + Name As Name, [Statistics Group Code], Ofrecimientos.Fecha "
+ ",Ofrecimientos.Periodicidad,(Select Top 1 OID From Reconocimientos where Reconocimientos.Empleado = CHSD$Employee.No_ Order By Fecha Desc) AS Reconocimiento "
 + "From Ofrecimientos inner join CHSD$Employee On Ofrecimientos.Empleado = CHSD$Employee.No_ "
 + "AND Ofrecimientos.Fecha > DATEADD(MONTH, -(Ofrecimientos.Periodicidad -2), GETDATE())" //  order by Fecha desc";
 + "union all "
 + "Select Distinct No_ , [First Family Name]+' ' +[Second Family Name] + ',' + Name As Name, [Statistics Group Code], Ofrecimientos.Fecha "
+ ",Ofrecimientos.Periodicidad, (Select Top 1 OID From Reconocimientos where Reconocimientos.Empleado = Ofrecimientos.Empleado Order By Fecha Desc) AS Reconocimiento "
+ " From Ofrecimientos inner join CHSD$Employee On Ofrecimientos.Empleado = CHSD$Employee.No_ "
 + "AND((Aceptado= 0 And NoAceptado = 0) AND Ofrecimientos.Fecha<GETDATE()) order by Fecha desc";
            //string sql = "SELECT CHSD$Employee.[First Family Name]+' '+CHSD$Employee.[Second Family Name]+','+ CHSD$Employee.Name As Name, CHSD$Employee.[Statistics Group Code] FROM Citas inner join CHSD$Employee on Citas.Empleado = CHSD$Employee.No_ Where (Select Count(OID) from Reconocimientos where Reconocimientos.Citas = Citas.OID) = 0 And IsNull(Citas.NoPresentado,0)= 0";

            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Empleados Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EmpleadosRestantes");
            daEpisRestantes.Fill(dsPubs, "EmpleadosRestantes");

            return dsPubs.Tables["EmpleadosRestantes"];
        }
    }
}
