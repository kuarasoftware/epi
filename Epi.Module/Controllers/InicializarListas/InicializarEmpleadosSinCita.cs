﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public partial class InicializarEmpleadosSinCita : WindowController
    {
        public InicializarEmpleadosSinCita() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosSinCita)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosSinCita> objects = new BindingList<EmpleadosSinCita>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                objects.Add(new EmpleadosSinCita() {
                    Empleado = (BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString()),
                    Ofrecimiento=(Ofrecimientos)Os.GetObjectByKey<Ofrecimientos>(drCurrent["OID"])
                });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();

            string sql = "SELECT DISTINCT CHSD$Employee.[No_],OID FROM Ofrecimientos inner join CHSD$Employee on Ofrecimientos.Empleado = CHSD$Employee.No_ Where ISNULL(Ofrecimientos.Aceptado,0) = 1 and (Select Count(Citas.OID) from Citas where Citas.FechaCita >= Ofrecimientos.Fecha AND Citas.Empleado = Ofrecimientos.Empleado) = 0 "+
                " and (Select Count(Solicitudes.OID) from Solicitudes where Solicitudes.Ofrecimiento = Ofrecimientos.OID) = 0 "+
                "AND (CONVERT(varchar(16),[Termination Date],102)>=CONVERT(varchar(16), GETDATE(), 102) OR isnull([Termination Date],'1753-01-01 00:00:00.000')='1753-01-01 00:00:00.000')";

            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Empleados Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EmpleadosRestantes");
            daEpisRestantes.Fill(dsPubs, "EmpleadosRestantes");

            return dsPubs.Tables["EmpleadosRestantes"];
        }
    }
}
