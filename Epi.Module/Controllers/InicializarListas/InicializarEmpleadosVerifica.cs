﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public partial class InicializarEmpleadosVerifica : WindowController
    {
        public InicializarEmpleadosVerifica() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosVerifica)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosVerifica> objects = new BindingList<EmpleadosVerifica>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                objects.Add(new EmpleadosVerifica()
                {
                    Empleado = (BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString()),
                    fechafinalcontrato = Convert.ToDateTime(drCurrent["Termination Date"].ToString()),
                    ultimoofrecimiento = Convert.ToDateTime(drCurrent["UltimoOfrecimiento"].ToString()),
                    
                    ultimacita = (Citas)Os.GetObjectByKey<Citas>(drCurrent["UltimaCita"]),
                    ultimoreconocimiento = (Reconocimientos)Os.GetObjectByKey<Reconocimientos>(drCurrent["UltimoReconocimiento"]),
                    proximoofrecimiento = (Ofrecimientos)Os.GetObjectByKey<Ofrecimientos>(drCurrent["ProximoOfrecimiento"])
                });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();

            string sql = "SELECT CHSD$Employee.[No_] , isnull(CHSD$Employee.[Termination Date],'1753-01-01 00:00:00.000') AS [Termination Date] , " +
"isnull((Select Top 1 Fecha From Ofrecimientos where Ofrecimientos.Empleado = CHSD$Employee.No_ and Fecha < (Select Max(Fecha) From Ofrecimientos where Ofrecimientos.Empleado = CHSD$Employee.No_) Order By Fecha Desc),0) As UltimoOfrecimiento," +
 "isnull((Select TOP 1 OID From Citas where Citas.Empleado = CHSD$Employee.No_ Order By FechaCita Desc),0) As UltimaCita," +
 "isnull((Select TOP 1 OID From Reconocimientos where Reconocimientos.Empleado = CHSD$Employee.No_ Order By Fecha Desc),0) As UltimoReconocimiento," +
 "isnull((Select Top 1 OID From Ofrecimientos where Ofrecimientos.Empleado = CHSD$Employee.No_ AND Fecha > (Select Max(Fecha) From Reconocimientos where Reconocimientos.Empleado = CHSD$Employee.No_) Order By Fecha Desc),0) As ProximoOfrecimiento  " +
 "FROM CHSD$Employee "+
"where(CONVERT(varchar(16),[Termination Date], 102) >= CONVERT(varchar(16), GETDATE(), 102) OR isnull([Termination Date],'1753-01-01 00:00:00.000') = '1753-01-01 00:00:00.000')";

            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Verificacion Empleados");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "VerificacionEmpleados");
            daEpisRestantes.Fill(dsPubs, "VerificacionEmpleados");

            return dsPubs.Tables["VerificacionEmpleados"];
        }
    }
}
