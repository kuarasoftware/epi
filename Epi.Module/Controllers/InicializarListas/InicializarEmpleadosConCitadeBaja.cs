﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public partial class InicializarEmpleadosConCitadeBaja : WindowController
    {
        public InicializarEmpleadosConCitadeBaja() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosConCitadeBaja)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosConCitadeBaja> objects = new BindingList<EmpleadosConCitadeBaja>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                objects.Add(new EmpleadosConCitadeBaja() {
                    Empleado = (BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString()),
                    Cita = (Citas)Os.GetObjectByKey<Citas>(drCurrent["OID"]),
                    Desde= Convert.ToDateTime(drCurrent["From Date"].ToString()),
                    Hasta =Convert.ToDateTime(drCurrent["To Date"].ToString()),
                    Causa=drCurrent["Cause of Absence Code"].ToString()
                    //FechaCita,[From Date],[To Date],[Cause of Absence Code]
                });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();

            string sql = "SELECT DISTINCT [CHSD$Employee Absence].[Employee No_] AS No_,OID,FechaCita,[From Date],[To Date],[Cause of Absence Code] FROM Citas inner join [CHSD$Employee Absence] on Citas.Empleado = [CHSD$Employee Absence].[Employee No_] "
                         + " and FechaCita>=[From Date] and FechaCita<= (CASE when[To Date] = '1753-01-01 00:00:00.000' THEN DATEADD(YEAR,10,GETDATE()) else [To Date] end)"
                         + " where FechaCita>=DATEADD(DAY,-1, GETDATE())";

            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Empleados Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EmpleadosRestantes");
            daEpisRestantes.Fill(dsPubs, "EmpleadosRestantes");

            return dsPubs.Tables["EmpleadosRestantes"];
        }
    }
}
