﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers.InicializarListas
{
    public partial class InicializarEmpleadosSinCitaPedida : WindowController
    {
        public InicializarEmpleadosSinCitaPedida() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EmpleadosSinCitaPedida)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            IObjectSpace Os = Application.CreateObjectSpace();
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EmpleadosSinCitaPedida> objects = new BindingList<EmpleadosSinCitaPedida>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                Solicitudes solicitudes = Os.GetObjectByKey<Solicitudes>(drCurrent["OID"]);
                objects.Add(new EmpleadosSinCitaPedida() {
                    Ofrecimientos =(Ofrecimientos)Os.GetObjectByKey<Ofrecimientos>(solicitudes.Ofrecimiento.Oid),
                    Empleado =(BusinessObjects.Temel.Employee)Os.GetObjectByKey<BusinessObjects.Temel.Employee>(drCurrent["No_"].ToString())
                });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();
            string sql = "SELECT DISTINCT CHSD$Employee.[No_],OID FROM Solicitudes inner join CHSD$Employee on Solicitudes.Empleado = CHSD$Employee.No_ " +
                "Where " +
                //"ISNULL(Ofrecimientos.CitaRecibida,0) = 0 AND ISNULL(Ofrecimientos.Aceptado,0) = 1 "+
                " (Select Count(Citas.OID) from Citas where Citas.FechaCita >= Solicitudes.Fecha  AND Citas.Empleado=Solicitudes.Empleado AND Isnull(NoPresentado,0)=0) = 0 and "+
                " Solicitudes.Comunicadoalamutua=1 "+
                "AND (CONVERT(varchar(16),[Termination Date],102)>=CONVERT(varchar(16), GETDATE(), 102) OR [Termination Date]='1753-01-01 00:00:00.000')";
            //string sql = "SELECT CHSD$Employee.[First Family Name]+' '+CHSD$Employee.[Second Family Name]+','+ CHSD$Employee.Name As Name, CHSD$Employee.[Statistics Group Code] FROM Citas inner join CHSD$Employee on Citas.Empleado = CHSD$Employee.No_ Where (Select Count(OID) from Reconocimientos where Reconocimientos.Citas = Citas.OID) = 0 And IsNull(Citas.NoPresentado,0)= 0";

            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Empleados Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EmpleadosRestantes");
            daEpisRestantes.Fill(dsPubs, "EmpleadosRestantes");

            return dsPubs.Tables["EmpleadosRestantes"];
        }
    }
}
