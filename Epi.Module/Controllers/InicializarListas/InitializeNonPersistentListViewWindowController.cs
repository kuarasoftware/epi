﻿using DevExpress.ExpressApp;
using Epi.Module.BusinessObjects;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Epi.Module.Controllers
{
    public class InitializeNonPersistentListViewWindowController : WindowController
    {
        public InitializeNonPersistentListViewWindowController() : base()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Application.ListViewCreating += Application_ListViewCreating;
        }
        private void Application_ListViewCreating(Object sender, ListViewCreatingEventArgs e)
        {
            if ((e.CollectionSource.ObjectTypeInfo.Type == typeof(EpisRestantes)) && (e.CollectionSource.ObjectSpace is NonPersistentObjectSpace))
            {
                ((NonPersistentObjectSpace)e.CollectionSource.ObjectSpace).ObjectsGetting += ObjectSpace_ObjectsGetting;
            }
        }
        private void ObjectSpace_ObjectsGetting(Object sender, ObjectsGettingEventArgs e)
        {
            DataTable tblEpisRestantes = ObtenerObjetos();
            BindingList<EpisRestantes> objects = new BindingList<EpisRestantes>();
            foreach (DataRow drCurrent in tblEpisRestantes.Rows)
            {
                objects.Add(new EpisRestantes() { Epi = drCurrent["Descripcion"].ToString(), Empleado = drCurrent["Name"].ToString(), Categoria = drCurrent["Statistics Group Code"].ToString(), Cantidad = Int32.Parse(drCurrent["Cantidad"].ToString()) });
            }
            e.Objects = objects;
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Application.ListViewCreating -= Application_ListViewCreating;
        }
        DataTable ObtenerObjetos()
        {
            SqlConnection objConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            objConn.Open();

            string sql = "SELECT [First Family Name]+' '+[Second Family Name]+','+ Name As Name, [Statistics Group Code], Epis.Descripcion, Cantidad FROM CHSD$Employee inner join EpisxCartegoria on EpisxCartegoria.Categoria = CHSD$Employee.[Statistics Group Code] inner join EpisCategoriaCantidad on EpisCategoriaCantidad.EpisxCartegoria = EpisxCartegoria.OID inner join Epis on Epis.OID = EpisCategoriaCantidad.Epis where EpisCategoriaCantidad.Estado = 0 except Select[First Family Name]+' ' +[Second Family Name] + ',' + Name As Name, [Statistics Group Code], Epis.Descripcion, Cantidad From EpisxEmpleadoEpis   inner join Epis on Epis.OID = EpisxEmpleadoEpis.Epis inner join EpisxEmpleado on EpisxEmpleado.OID = EpisxEmpleadoEpis.EpisxEmpleado inner join CHSD$Employee on CHSD$Employee.No_ = EpisxEmpleado.Empleado where EpisxEmpleadoEpis.Estado = 0";
            SqlDataAdapter daEpisRestantes = new SqlDataAdapter(sql, objConn);
            DataSet dsPubs = new DataSet("Epis Restantes");
            daEpisRestantes.FillSchema(dsPubs, SchemaType.Source, "EpisRestantes");
            daEpisRestantes.Fill(dsPubs, "EpisRestantes");

            DataTable tblEpisRestantes;
            return tblEpisRestantes = dsPubs.Tables["EpisRestantes"];
        }
    }
}
