﻿using System;
using DevExpress.ExpressApp.Actions;

namespace Epi.Module.Controllers
{
    partial class BotonesDocumentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AbrirDocumentos = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InsertarNuevoDocumento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AbrirDocumento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearCita = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearReconocimiento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CrearCitadesdeSolicitud = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.actualizarsolicitudes = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.VerReconocimiento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AbrirDocumentos
            // 
            this.AbrirDocumentos.Caption = "Abrir Documentos";
            this.AbrirDocumentos.ConfirmationMessage = null;
            this.AbrirDocumentos.Id = "AbrirDocumentos";
            this.AbrirDocumentos.ImageName = "Navision.CopyDocument";
            this.AbrirDocumentos.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.AbrirDocumentos.TargetObjectsCriteria = "";
            this.AbrirDocumentos.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.AbrirDocumentos.ToolTip = null;
            this.AbrirDocumentos.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.AbrirDocumentos.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AbrirListaDocumentos_Execute);
            // 
            // InsertarNuevoDocumento
            // 
            this.InsertarNuevoDocumento.Caption = "Insertar Nuevo Documento";
            this.InsertarNuevoDocumento.ConfirmationMessage = null;
            this.InsertarNuevoDocumento.Id = "InsertarNuevoDocumento";
            this.InsertarNuevoDocumento.ImageName = "Navision.FindCreditMemo";
            this.InsertarNuevoDocumento.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.InsertarNuevoDocumento.TargetObjectType = typeof(Epi.Module.BusinessObjects.Documento);
            this.InsertarNuevoDocumento.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InsertarNuevoDocumento.ToolTip = null;
            this.InsertarNuevoDocumento.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InsertarNuevoDocumento.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InsertarNuevoDocumento_Execute);
            // 
            // AbrirDocumento
            // 
            this.AbrirDocumento.Caption = "Abrir Documento";
            this.AbrirDocumento.ConfirmationMessage = null;
            this.AbrirDocumento.Id = "AbrirDocumento";
            this.AbrirDocumento.ImageName = "Navision.Document";
            this.AbrirDocumento.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.AbrirDocumento.TargetObjectType = typeof(Epi.Module.BusinessObjects.Documento);
            this.AbrirDocumento.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.AbrirDocumento.ToolTip = null;
            this.AbrirDocumento.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.AbrirDocumento.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AbrirDocumento_Execute);
            // 
            // CrearCita
            // 
            this.CrearCita.Caption = "Crear Cita";
            this.CrearCita.ConfirmationMessage = null;
            this.CrearCita.Id = "CrearCita";
            this.CrearCita.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.CrearCita.TargetObjectType = typeof(Epi.Module.BusinessObjects.EmpleadosSinCitaPedida);
            this.CrearCita.ToolTip = null;
            this.CrearCita.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearCita_Execute);
            // 
            // CrearReconocimiento
            // 
            this.CrearReconocimiento.Caption = "Insertar Reconocimiento";
            this.CrearReconocimiento.ConfirmationMessage = null;
            this.CrearReconocimiento.Id = "CrearReconocimiento";
            this.CrearReconocimiento.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.CrearReconocimiento.TargetObjectType = typeof(Epi.Module.BusinessObjects.Citas);
            this.CrearReconocimiento.ToolTip = null;
            this.CrearReconocimiento.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearReconocimiento_Execute);
            // 
            // CrearCitadesdeSolicitud
            // 
            this.CrearCitadesdeSolicitud.Caption = "Crear Cita Desde Solicitud";
            this.CrearCitadesdeSolicitud.ConfirmationMessage = null;
            this.CrearCitadesdeSolicitud.Id = "CrearCitaDesdeSolicitud";
            this.CrearCitadesdeSolicitud.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.CrearCitadesdeSolicitud.TargetObjectType = typeof(Epi.Module.BusinessObjects.Solicitudes);
            this.CrearCitadesdeSolicitud.ToolTip = null;
            this.CrearCitadesdeSolicitud.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearCitadesdesolicitud_Execute);
            // 
            // actualizarsolicitudes
            // 
            this.actualizarsolicitudes.Caption = "Borrar Solicitud";
            this.actualizarsolicitudes.ConfirmationMessage = null;
            this.actualizarsolicitudes.Id = "CrearsolicitudDesdeSolicitud";
            this.actualizarsolicitudes.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.actualizarsolicitudes.TargetObjectType = typeof(Epi.Module.BusinessObjects.Ofrecimientos);
            this.actualizarsolicitudes.ToolTip = null;
            this.actualizarsolicitudes.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Actualizarsolicitudes_Execute);
            // 
            // VerReconocimiento
            // 
            this.VerReconocimiento.Caption = "Ver Reconocimiento";
            this.VerReconocimiento.ConfirmationMessage = null;
            this.VerReconocimiento.Id = "VerReconocimiento";
            this.VerReconocimiento.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.VerReconocimiento.TargetViewId = "Ofrecimientos_ListView;Ofrecimientos_ListView_Listview;Ofrecimientos_DetailView;E" +
    "mpleadosVerifica_ListView;Citas_ListView;Citas_ListView_Listview;Citas_DetailVie" +
    "w";
            this.VerReconocimiento.ToolTip = null;
            this.VerReconocimiento.Execute += VerReconocimiento_Execute;
            // 
            // BotonesDocumentos
            // 
            this.Actions.Add(this.AbrirDocumentos);
            this.Actions.Add(this.InsertarNuevoDocumento);
            this.Actions.Add(this.AbrirDocumento);
            this.Actions.Add(this.CrearCita);
            this.Actions.Add(this.CrearReconocimiento);
            this.Actions.Add(this.CrearCitadesdeSolicitud);
            this.Actions.Add(this.actualizarsolicitudes);
            this.Actions.Add(this.VerReconocimiento);

        }

        private void VerReconocimiento_Execute1(object sender, SimpleActionExecuteEventArgs e)
        {
            throw new NotImplementedException();
        }





        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InsertarNuevoDocumento;
        private DevExpress.ExpressApp.Actions.SimpleAction AbrirDocumentos;
        private DevExpress.ExpressApp.Actions.SimpleAction AbrirDocumento;
        private DevExpress.ExpressApp.Actions.SimpleAction CrearCita;
        private DevExpress.ExpressApp.Actions.SimpleAction CrearReconocimiento;
        private SimpleAction CrearCitadesdeSolicitud;
        private SimpleAction actualizarsolicitudes;
        private SimpleAction VerReconocimiento;
    }
}