﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Layout;
using DevExpress.XtraPdfViewer;
using DevExpress.XtraRichEdit;
using Epi.Module.BusinessObjects;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{
    public partial class VerDocumentos : ViewController
    {
        public VerDocumentos()
        {
            InitializeComponent();
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            IniciarVistaDocumento();
        }
        public void IniciarVistaDocumento()
        {
            //switch (((Documento)View.CurrentObject).Extension)
            //{
            //    case ".rtf":
            //    case ".xml":
            //    case ".docx":
            //    case ".doc":
            CrearControlador("VerDocumentoRichEditControl");
            //    break;
            //case ".pdf":
            CrearControlador("VerDocumentoPDFViewer");
            //    break;
            //default:
            CrearControlador("VerDocumentoWebBrowser");
            //        break;
            //}
        }
        public void item_ControlCreated(object sender, EventArgs e)
        {
            try
            {
                switch (((Documento)View.CurrentObject).Extension)
                {
                    case ".rtf":
                    case ".xml":
                    case ".docx":
                    case ".doc":
                        (((ControlViewItem)sender).Control as RichEditControl).ReadOnly = true;
                        if (((Documento)View.CurrentObject).Anexar == false)
                        {
                            try
                            {
                                (((ControlViewItem)sender).Control as RichEditControl).LoadDocument(((Documento)View.CurrentObject).RutaFisica);
                            }
                            catch
                            {
                                MessageBox.Show("No se encuentra el fichero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            (((ControlViewItem)sender).Control as RichEditControl).OpenXmlBytes = ((Documento)View.CurrentObject).IDOCUMENTO;
                        }
                        break;
                    case ".pdf":
                        (((ControlViewItem)sender).Control as PdfViewer).LoadDocument(((Documento)View.CurrentObject).RutaFisica);
                        break;
                    default:
                        (((ControlViewItem)sender).Control as WebBrowser).Navigate(((Documento)View.CurrentObject).RutaFisica);
                        break;
                }
            }
            catch
            {

            }
        }
        private void CrearControlador(string s)
        {
            ControlViewItem item = ((DetailView)View).FindItem(s) as ControlViewItem;
            if (item != null)
            {
                if (item.Control != null)
                {
                    item_ControlCreated(item, EventArgs.Empty);
                }
                else
                {
                    item.ControlCreated += item_ControlCreated;
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();

        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        private void VisualizarDocumento_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {
            IniciarVistaDocumento();
        }
    }
}
