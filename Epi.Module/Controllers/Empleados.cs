﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;
using System;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Empleados : ViewController<ListView>
    {
        public Empleados()
        {
            TargetViewId = "Employee_LookupListView";
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }


        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<FilterController>().CustomBuildCriteria += Empleados_CustomBuildCriteria;
            //View.SelectionChanged += View_SelectionChanged;
            // Perform various tasks depending on the target View.
        }

        

        

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
            Frame.GetController<FilterController>().CustomBuildCriteria -= Empleados_CustomBuildCriteria;
        }
        void Empleados_CustomBuildCriteria(object sender, CustomBuildCriteriaEventArgs e)
        {
            if (e.SearchText != null) e.Criteria = CriteriaOperator.Parse("Contains(Upper(No_), ?) | Contains(Upper(Name), ?) | Contains(Upper(FirstFamilyName), ?) | Contains(Upper(SecondFamilyName), ?) | Contains(Upper(StatisticsGroupCode), ?) | Contains(Upper(NombreCompleto), ?)", e.SearchText.ToUpper(), e.SearchText.ToUpper(), e.SearchText.ToUpper(), e.SearchText.ToUpper(), e.SearchText.ToUpper(), e.SearchText.ToUpper());
            e.Handled = true;
            return;


        }
    }
}
