﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Layout;
using DevExpress.ExpressApp.Win.SystemModule;
using DevExpress.Utils;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Epi.Module.Controllers
{
    public partial class ExpandableLayoutGroupViewController : ViewController<DetailView>, IModelExtender
    {
        Dictionary<string, IModelWinLayoutGroupExtender> itemToWinModelLayoutGroupExtenderMap = new Dictionary<string, IModelWinLayoutGroupExtender>();
        WinLayoutManager winLayoutManager = null;
        protected override void OnActivated()
        {
            base.OnActivated();
            winLayoutManager = View.LayoutManager as WinLayoutManager;
            if (winLayoutManager != null)
            {
                winLayoutManager.ItemCreated += ExpandableLayoutGroupViewControllercs_ItemCreated;
                if (winLayoutManager.Container != null)
                {
                    winLayoutManager.Container.HandleCreated += Container_HandleCreated;
                }
                View.ModelSaved += View_ModelSaved;
            }
        }
        void Container_HandleCreated(object sender, EventArgs e)
        {
            LayoutControl lc = ((LayoutControl)sender);
            lc.BeginUpdate();
            foreach (BaseLayoutItem item in lc.Items)
            {
                if ((item is LayoutControlGroup) && itemToWinModelLayoutGroupExtenderMap.ContainsKey(item.Name))
                {
                    ((LayoutGroup)item).Expanded = itemToWinModelLayoutGroupExtenderMap[item.Name].Expanded;
                    ((LayoutGroup)item).HeaderButtonsLocation = itemToWinModelLayoutGroupExtenderMap[item.Name].HeaderButtonsLocation;
                    ((LayoutGroup)item).ExpandButtonVisible = true;
                    ((LayoutGroup)item).ExpandOnDoubleClick = true;
                }
            }
            lc.EndUpdate();
        }
        void ExpandableLayoutGroupViewControllercs_ItemCreated(object sender, ItemCreatedEventArgs e)
        {
            if (e.ModelLayoutElement is IModelWinLayoutGroup)
            {
                IModelWinLayoutGroupExtender modelLayoutGroupExtender = (IModelWinLayoutGroupExtender)e.ModelLayoutElement;
                if ((modelLayoutGroupExtender).Expandable)
                {
                    itemToWinModelLayoutGroupExtenderMap.Add(e.Item.Name, (IModelWinLayoutGroupExtender)e.ModelLayoutElement);
                }
            }
        }
        void View_ModelSaved(object sender, EventArgs e)
        {
            foreach (BaseLayoutItem item in winLayoutManager.Container.Items)
            {
                if ((item is LayoutControlGroup) && itemToWinModelLayoutGroupExtenderMap.ContainsKey(item.Name))
                {
                    itemToWinModelLayoutGroupExtenderMap[item.Name].Expanded = ((LayoutGroup)item).Expanded;
                }
            }
        }
        protected override void OnDeactivated()
        {
            if (winLayoutManager != null)
            {
                winLayoutManager.ItemCreated -= ExpandableLayoutGroupViewControllercs_ItemCreated;
                if (winLayoutManager.Container != null)
                {
                    winLayoutManager.Container.HandleCreated -= Container_HandleCreated;
                    winLayoutManager = null;
                }
                View.ModelSaved -= View_ModelSaved;
            }
            itemToWinModelLayoutGroupExtenderMap.Clear();
            base.OnDeactivated();
        }

        public void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
        {
            extenders.Add<IModelWinLayoutGroup, IModelWinLayoutGroupExtender>();
        }
    }
    public interface IModelWinLayoutGroupExtender
    {
        [DefaultValue(true)]
        bool Expanded { get; set; }
        [DefaultValue(true)]
        bool Expandable { get; set; }
        [DefaultValue(GroupElementLocation.Default)]
        GroupElementLocation HeaderButtonsLocation { get; set; }
    }
}
