﻿namespace Epi.Module.Controllers
{
    partial class VerDocumentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VisualizarDocumento = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // VisualizarDocumento
            // 
            this.VisualizarDocumento.Caption = "Visualizar Documento";
            this.VisualizarDocumento.Category = "Actualizar";
            this.VisualizarDocumento.ConfirmationMessage = null;
            this.VisualizarDocumento.Id = "VisualizarDocumento";
            this.VisualizarDocumento.ImageName = "Action_Refresh";
            this.VisualizarDocumento.TargetObjectType = typeof(Epi.Module.BusinessObjects.Documento);
            this.VisualizarDocumento.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.VisualizarDocumento.ToolTip = null;
            this.VisualizarDocumento.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.VisualizarDocumento.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.VisualizarDocumento_Execute);
            // 
            // VerDocumentos
            // 
            this.Actions.Add(this.VisualizarDocumento);
            this.TargetObjectType = typeof(Epi.Module.BusinessObjects.Documento);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction1;
        private DevExpress.ExpressApp.Actions.SimpleAction VisualizarDocumento;
    }
}