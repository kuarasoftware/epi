﻿namespace Epi.Module.Controllers
{
    partial class BotonesCitas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CrearInformeCita = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.VerCita = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CrearInformeCita
            // 
            this.CrearInformeCita.Caption = "Crear Informe Cita";
            this.CrearInformeCita.ConfirmationMessage = null;
            this.CrearInformeCita.Id = "CrearInformeCita";
            this.CrearInformeCita.TargetObjectType = typeof(Epi.Module.BusinessObjects.Citas);
            this.CrearInformeCita.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CrearInformeCita.ToolTip = null;
            this.CrearInformeCita.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CrearInformeCita.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearInformeCita_Execute);
            // 
            // VerCita
            // 
            this.VerCita.Caption = "Ver Cita";
            this.VerCita.ConfirmationMessage = null;
            this.VerCita.Id = "VerCita";
            this.VerCita.TargetViewId = "Solicitudes_ListView;Solicitudes_DatailView;Ofrecimientos_ListView;Ofrecimientos_ListView_Listview;Ofrecimientos_DetailView;EmpleadosSinReconocimientos_ListView;EmpleadosVerifica_ListView;Reconocimientos_ListView;Reconocimientos_ListView_ListView;Reconocimientos_DetailView;EmpleadosConCitadeBaja_ListView";
            this.VerCita.ToolTip = null;
            this.VerCita.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.VerCita.Execute += VerCita_Execute;
            // 
            // BotonesCitas
            // 
            this.Actions.Add(this.CrearInformeCita);
            this.Actions.Add(this.VerCita);

        }

        

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CrearInformeCita;
        private DevExpress.ExpressApp.Actions.SimpleAction VerCita;
    }
}
