﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;
using Epi.Module.BusinessObjects;
using Epi.Module.Utils;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Epi.Module.Controllers
{

    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BotonesDocumentos : ViewController
    {
        public static string TABLADOCUMENTO;
        public static long OIDDOCUMENTO;

        public BotonesDocumentos()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            if (Frame.GetController<DevExpress.ExpressApp.SystemModule.RecordsNavigationController>().Actions != null)
            {
                foreach (var item in Frame.GetController<DevExpress.ExpressApp.SystemModule.RecordsNavigationController>().Actions)
                {
                    switch (item.Id)
                    {
                        case "PreviousObject":
                            item.Caption = "Anterior";
                            break;
                        case "NextObject":
                            item.Caption = "Siguiente";
                            break;
                        default:
                            break;
                    }
                }
            }
            if (Frame.GetController<DevExpress.ExpressApp.SystemModule.ResetViewSettingsController>().Actions != null)
            {
                foreach (var item in Frame.GetController<DevExpress.ExpressApp.SystemModule.ResetViewSettingsController>().Actions)
                {
                    switch (item.Id)
                    {
                        case "ResetViewSettings":
                            try
                            {
                                item.Active["Controller active"] = false;
                            }
                            catch
                            { }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            //bool verboton = this.View.Id != "Documento_DetailView";
            //AbrirDocumentos.Enabled.SetItemValue("bool", verboton);
            if ((this.View.Id == "Documento_DetailView") || (this.View.Id == "PlantillasWord_DetailView"))
            {
                AbrirDocumentos.Dispose();
            }
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Este metodo hacer referencia al boton que sale en todas las vistas detailview y sirve
        /// para abrir la lista de documentos
        /// </summary>
        private void VerReconocimiento_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            if (View.CurrentObject == null)
            {
                MessageBox.Show("Seleccione una fila", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            IObjectSpace objectSpace;
            Reconocimientos reconocimiento;
            
            switch (View.Id)
            {
                case "Ofrecimientos_ListView_Listview":
                case "Ofrecimientos_ListView":
                case "Ofrecimientos_DetailView":
                   
                    Ofrecimientos ofr = (Ofrecimientos)View.CurrentObject;
                    XPCollection<Citas> citas = new XPCollection<Citas>(ofr.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimientos]=?", ofr.Oid));
                    if ((citas == null) || (citas.Count == 0))
                    {
                        MessageBox.Show("No hay ningun reconocimiento para este ofrecimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    XPCollection<Reconocimientos> recos = new XPCollection<Reconocimientos>(ofr.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Citas]=?", citas[0].Oid));
                    if ((recos == null) || (recos.Count == 0))
                    {
                        MessageBox.Show("No hay ningun reconocimiento para este ofrecimiento", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    reconocimiento = recos[0];
                    objectSpace = Application.CreateObjectSpace(typeof(Reconocimientos));
                    DetailView detailView1 = this.Application.CreateDetailView(View.ObjectSpace, reconocimiento, false);
                    detailView1.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.View;
                    e.ShowViewParameters.CreatedView = detailView1;
                    break;
                
                case "EmpleadosVerifica_ListView":
                    EmpleadosVerifica ver = (EmpleadosVerifica)View.CurrentObject;
                    if (ver.ultimoreconocimiento == null)
                    {
                        MessageBox.Show("No hay ningun reconocimiento para este empleado", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                    reconocimiento = ver.ultimoreconocimiento;
                    objectSpace = Application.CreateObjectSpace(typeof(Reconocimientos));
                    DetailView detailView2 = this.Application.CreateDetailView(View.ObjectSpace, reconocimiento, false);
                    detailView2.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView2;
                    break;
                //Citas_ListView;Citas_ListView_Listview;Citas_DetailView;EmpleadosConCitadeBaja;
                case "Citas_ListView_ListView":
                case "Citas_ListView":
                case "Citas_DetailView":

                    Citas cita = (Citas)View.CurrentObject;
                    XPCollection<Reconocimientos> recos2 = new XPCollection<Reconocimientos>(cita.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Citas]=?", cita.Oid));
                    if ((recos2 == null) || (recos2.Count == 0))
                    {
                        MessageBox.Show("No hay ningun reconocimiento para esta cita", View.Caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    reconocimiento = recos2[0];
                    objectSpace = Application.CreateObjectSpace(typeof(Reconocimientos));
                    DetailView detailView3 = this.Application.CreateDetailView(View.ObjectSpace, reconocimiento, false);
                    detailView3.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                    e.ShowViewParameters.CreatedView = detailView3;
                    break;
                
            }

        }
        private void AbrirListaDocumentos_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            if (((this.View.CurrentObject) as XPObject).Oid != -1)
            {
                //abrir nueva ventana
                IObjectSpace objectSpace = Application.CreateObjectSpace();
                string listViewId = Application.FindListViewId(typeof(Documento));

                e.ShowViewParameters.CreatedView = Application.CreateListView(listViewId, Application.CreateCollectionSource(objectSpace,
                    typeof(Documento), listViewId), true);

                e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
                e.ShowViewParameters.Controllers.Add(Application.CreateController<ViewController>());
                //transmitir datos
                TABLADOCUMENTO = this.View.Id;
                OIDDOCUMENTO = ((this.View.CurrentObject) as XPObject).Oid;


            }
            else
            {
                MessageBox.Show("Debes guardar antes de poder abrir documentos.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        /// <summary>
        /// Este metodo hacer referencia al que sale en la vista de detail view de documento
        /// y sirve para abrir el documento guardado en la base de datos como un byte array
        /// </summary>
        private void AbrirDocumento_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            CrearDocumento.AbrirFichero((Documento)View.CurrentObject);
        }

        private void InsertarNuevoDocumento_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (this.View.Id == "PlantillasWord_DetailView")
            {
                CrearDocumento.CodificarArchivo(View, true, "Documentos Word (*.docx) | *.docx;");
            }
            else
            {
                if (MessageBox.Show("¿Quieres anexar el documento en el servidor?", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                {
                    ((Documento)View.CurrentObject).Anexar = false;
                }
                else
                {
                    ((Documento)View.CurrentObject).Anexar = true;
                }
                CrearDocumento.CodificarArchivo(View, ((Documento)View.CurrentObject).Anexar, string.Empty);
            }
        }
        private void CrearReconocimiento_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (e.SelectedObjects.Count == 0)
            {
                MessageBox.Show("Seleccione una cita");
                return;
            }
            Citas cita = (Citas)e.CurrentObject;
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Reconocimientos));
            Reconocimientos reco = (objectSpace.CreateObject(typeof(Reconocimientos))) as Reconocimientos;
            reco.AllDay = false;
            reco.Description = cita.Description;
            reco.Empleado = objectSpace.GetObjectByKey<BusinessObjects.Temel.Employee>(cita.Empleado.No_);
            reco.EndOn = cita.EndOn;
            reco.Fecha = DateTime.Now;
            reco.Label = 2;
            reco.Location = cita.Location;
            reco.TipoReconocimiento = cita.TipoReconocimiento;
            reco.Obsevaciones = cita.Obsevaciones;
            reco.Citas = objectSpace.GetObjectByKey<Citas>(cita.Oid);
            reco.StartOn = cita.StartOn;
            reco.Status = cita.Status;
            reco.Type = cita.Type;
            try
            {
                reco.Periodicidad = cita.Ofrecimientos.Periodicidad;
            }
            catch
            {
                reco.Periodicidad = 12;
            }
            
            reco.Save();

            DetailView detailView = this.Application.CreateDetailView(objectSpace, reco);
            detailView.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = detailView;
        }
        private void Actualizarsolicitudes_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //SimpleAction sim = (SimpleAction)sender;
            //IObjectSpace os = sim.Application.CreateObjectSpace();
            //Solicitudes solicitud = (Solicitudes)os.CreateObject<Solicitudes>();
            //XPCollection<Ofrecimientos> Aceptados = new XPCollection<Ofrecimientos>(solicitud.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Aceptado] = ? And [CitaRecibida]!=? And [SolicitudRealizada]!=?", true, true, true));
            //solicitud.Delete();
            //foreach (Ofrecimientos ofrecimientos in Aceptados)
            //{
            //    if (ofrecimientos.Fecha != DateTime.MinValue & ofrecimientos.Fecha!=null)
            //    {
            //        try
            //        {
            //            XPCollection<Citas> citacreada = new XPCollection<Citas>(ofrecimientos.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Empleado] = ? And [FechaCita] >=? AND [CitaAnulada]!=?", ofrecimientos.Empleado, ofrecimientos.Fecha, true));
            //            XPCollection<Reconocimientos> reconocimientos = new XPCollection<Reconocimientos>(ofrecimientos.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Empleado] = ? And [Fecha] >=?", ofrecimientos.Empleado, ofrecimientos.Fecha));
            //            if ((reconocimientos.Count == 0) || (reconocimientos == null))
            //            {
            //                if ((ofrecimientos.Empleado)!=null)
            //                {
            //                    ofrecimientos.SolicitudRealizada = true;
            //                    ofrecimientos.Automatico = true;
            //                    ofrecimientos.Save();
            //                    Solicitudes solicitudes = new Solicitudes(ofrecimientos.Session);
            //                    solicitudes.CitaRecibida = false;
            //                    if (citacreada.Count != 0) solicitudes.Cita = citacreada[0];
            //                    solicitudes.Description = ofrecimientos.Description;
            //                    solicitudes.Empleado = (BusinessObjects.Temel.Employee)os.GetObjectByKey<BusinessObjects.Temel.Employee>(ofrecimientos.Empleado.No_);
            //                    solicitudes.Fecha = DateTime.Now;
            //                    solicitudes.Obsevaciones = ofrecimientos.Obsevaciones;
            //                    solicitudes.Ofrecimiento = (Ofrecimientos)os.GetObjectByKey<Ofrecimientos>(ofrecimientos.Oid);
            //                    solicitudes.Save();
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show("Se ha producido el siguiente error: " + ex.ToString());
            //        }
            //    }
            //};
            //os.CommitChanges();
            SimpleAction sim = (SimpleAction)sender;
            IObjectSpace os = sim.Application.CreateObjectSpace();
            Ofrecimientos ofrecimientos = (Ofrecimientos)e.CurrentObject;
            //Solicitudes solicitud = (Solicitudes)os.CreateObject<Solicitudes>();
            XPCollection<Solicitudes> solicitudant = new XPCollection<Solicitudes>(ofrecimientos.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimiento.Oid] =?", ofrecimientos.Oid));
            if ((solicitudant.Count != 0) & (solicitudant != null))
            {

                for (int t = 0; t <= solicitudant.Count - 1; t++)
                {
                    solicitudant[t].Delete();
                }
            }
            //solicitud.Delete();
            os.CommitChanges();
        }
        private void CrearCita_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (e.SelectedObjects.Count == 0)
            {
                MessageBox.Show("Seleccione un ofrecimiento");
                return;
            }
            EmpleadosSinCitaPedida empleadosSinCitaPedida = (EmpleadosSinCitaPedida)e.CurrentObject;
            Ofrecimientos ofre = empleadosSinCitaPedida.Ofrecimientos;
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Citas));
            Citas citas = (objectSpace.CreateObject(typeof(Citas))) as Citas;
            citas.AllDay = false;
            citas.FechaCita = DateTime.Today;
            citas.FechaEntrega = DateTime.Now;
            citas.Description = ofre.Description;
            citas.Empleado = objectSpace.GetObjectByKey<BusinessObjects.Temel.Employee>(ofre.Empleado.No_);
            //citas.EndOn = ofre.EndOn;
            //citas.FechaCita = ofre.Fecha;
            //citas.FechaEntrega = null;// DateTime.Now;
            citas.Label = 1;
            citas.Location = ofre.Location;
            citas.Obsevaciones = ofre.Obsevaciones;
            citas.Ofrecimientos = objectSpace.GetObjectByKey<Ofrecimientos>(ofre.Oid);
            //citas.StartOn = ofre.StartOn;
            citas.Status = ofre.Status;
            citas.Type = ofre.Type;
            if (ofre.TipoOfrecimiento == null)
            {
                citas.TipoReconocimiento = objectSpace.GetObjectByKey<BusinessObjects.TipoReconocimiento>(1);
            }
            else
            {
                citas.TipoReconocimiento = objectSpace.GetObjectByKey<BusinessObjects.TipoReconocimiento>(ofre.TipoOfrecimiento.Oid);
            }
            citas.Save();
            ofre.SolicitudRealizada = true;
            ofre.CitaRecibida = true;
            ofre.Automatico = true;
            ofre.Save();
            XPCollection<Solicitudes> solicitudes = new XPCollection<Solicitudes>(ofre.Session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[Ofrecimiento] = ? ", ofre.Oid));
            foreach (Solicitudes sol in solicitudes)
                {
                sol.CitaRecibida = true;
                sol.Cita =View.ObjectSpace.GetObjectByKey<Citas>(citas.Oid);
                sol.Save();
            }
            View.ObjectSpace.CommitChanges();
            DetailView detailView = this.Application.CreateDetailView(objectSpace, citas);
            detailView.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = detailView;
            
        }
        private void CrearCitadesdesolicitud_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            
            if (e.SelectedObjects.Count == 0)
            {
                MessageBox.Show("Seleccione una solicitud");
                return;
            }
            Solicitudes  solicitudes = (Solicitudes)e.CurrentObject;
            Ofrecimientos ofre = solicitudes.Ofrecimiento;
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Citas));
            Citas citas = (objectSpace.CreateObject(typeof(Citas))) as Citas;
            if ((solicitudes.Cita == null) || (solicitudes.Cita.Oid==-1))
            {
             
                citas.AllDay = false;
                citas.Description = ofre.Description;
                citas.Empleado = objectSpace.GetObjectByKey<BusinessObjects.Temel.Employee>(ofre.Empleado.No_);
                //citas.EndOn = ofre.EndOn;
                //citas.FechaCita = ofre.Fecha;
                //citas.FechaEntrega = null;// DateTime.Now;
                citas.Label = 1;
                citas.Location = ofre.Location;
                citas.Obsevaciones = ofre.Obsevaciones;
                citas.Ofrecimientos = objectSpace.GetObjectByKey<Ofrecimientos>(ofre.Oid);
                //citas.StartOn = ofre.StartOn;
                citas.Status = ofre.Status;
                citas.Type = ofre.Type;
                if (ofre.TipoOfrecimiento == null)
                {
                    citas.TipoReconocimiento = objectSpace.GetObjectByKey<BusinessObjects.TipoReconocimiento>(1);
                }
                else
                {
                    citas.TipoReconocimiento = ofre.FinalidadOfrecimiento;
                }
                citas.Save();
                solicitudes.Cita =View.ObjectSpace.GetObjectByKey<Citas>(citas.Oid);
                solicitudes.Save();
            }
            else
            {
                citas = solicitudes.Cita;
            }
            DetailView detailView = this.Application.CreateDetailView(objectSpace, citas);
            detailView.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = detailView;

        }
    }
    

}


