﻿namespace Epi.Module.Controllers
{
    partial class BotonesConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CrearInformeConsulta = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CrearInformeConsulta
            // 
            this.CrearInformeConsulta.Caption = "Crear Informe Consulta";
            this.CrearInformeConsulta.ConfirmationMessage = null;
            this.CrearInformeConsulta.Id = "CrearInformeConsulta";
            this.CrearInformeConsulta.TargetObjectType = typeof(Epi.Module.BusinessObjects.Consultas);
            this.CrearInformeConsulta.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CrearInformeConsulta.ToolTip = null;
            this.CrearInformeConsulta.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CrearInformeConsulta.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CrearInformeCita_Execute);
            // 
            // BotonesConsultas
            // 
            this.Actions.Add(this.CrearInformeConsulta);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CrearInformeConsulta;
    }
}
