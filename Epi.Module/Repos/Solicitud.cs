﻿using DevExpress.ExpressApp;
using DevExpress.XtraReports.UI;
using Epi.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Epi.Module.Repos
{
    public partial class Solicitud : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Constructor para una única solicitud
        /// </summary>
        /// <param name="currentObject"></param>
        public Solicitud(BusinessObjects.Solicitudes currentObject)
        {
            InitializeComponent();

            Nombre.Text = currentObject.Empleado.Name;
            Apellido1.Text = currentObject.Empleado.FirstFamilyName;
            Apellido2.Text = currentObject.Empleado.SecondFamilyName;
            Puesto.Text = currentObject.Empleado.Puesto;
            FechaNacimiento.Text = currentObject.Empleado.BirthDate.ToShortDateString();
            FechaAlta.Text= currentObject.Empleado.TerminationDate.ToShortDateString();
            if (currentObject.Empleado.NIE != null)
            {
                DNI.Text = currentObject.Empleado.NIE;
            }
            else
            {
                DNI.Text = currentObject.Empleado.DNI;
            }
            Centro.Text = "Hospital Universitari Son Espases";
            //Entregado.Value = currentObject.Entregado;
        }

        /// <summary>
        /// Constructor para crear el reporte para una o más solicitudes 
        /// </summary>
        /// <param name="view"></param>
        public Solicitud(View view)
        {
            InitializeComponent();

            bool PrimeraRowLlena = false;

            foreach(var obj in view.SelectedObjects)
            {
                if (!PrimeraRowLlena)
                {
                    if (((Solicitudes)obj).Empleado.DNI != null) {
                        this.DNI.Text = ((Solicitudes)obj).Empleado.DNI;
                }
                else {
                        this.DNI.Text = ((Solicitudes)obj).Empleado.NIE;
                }
                    this.Nombre.Text = ((Solicitudes)obj).Empleado.Name;
                    this.Apellido1.Text = ((Solicitudes)obj).Empleado.FirstFamilyName;
                    this.Apellido2.Text = ((Solicitudes)obj).Empleado.SecondFamilyName;
                    this.FechaNacimiento.Text = ((Solicitudes)obj).Empleado.BirthDate.ToShortDateString();
                    this.FechaAlta.Text = ((Solicitudes)obj).Empleado.EmploymentDate.ToShortDateString();
                    this.Puesto.Text = ((Solicitudes)obj).Empleado.Puesto.ToString();
                    this.Centro.Text = "Hospital Universitari Son Espases";
                    PrimeraRowLlena = true;
                }
                else
                {
                    this.insertarRow((Solicitudes)obj);
                }
            }
        }

        private void insertarRow(Solicitudes solicitud)
        {
            var row = this.xrTable1.InsertRowBelow(xrTable1.Rows[0]);

            if ((solicitud.Empleado.DNI != null) && (solicitud.Empleado.DNI !=""))
            {
                row.Cells[0].Text = solicitud.Empleado.DNI;
            }
            else
            {
                row.Cells[0].Text = solicitud.Empleado.NIE;
            }
            row.Cells[1].Text = solicitud.Empleado.Name;
            row.Cells[2].Text = solicitud.Empleado.FirstFamilyName;
            row.Cells[3].Text = solicitud.Empleado.SecondFamilyName;
            row.Cells[4].Text = solicitud.Empleado.BirthDate.ToShortDateString();
            row.Cells[5].Text = solicitud.Empleado.Puesto.ToString();
            row.Cells[6].Text = "Hospital Universitari Son Espases";
            row.Cells[7].Text = solicitud.Empleado.EmploymentDate.ToShortDateString();
        }

    }
}
