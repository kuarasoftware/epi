﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Epi.Module.Repos
{
    public partial class Consulta : DevExpress.XtraReports.UI.XtraReport
    {
        public Consulta(BusinessObjects.Consultas currentObject)
        {
            InitializeComponent();
            DNI.Text = currentObject.Empleado.DNI;
            DNI2.Text = currentObject.Empleado.DNI;
            NOMBRE.Text = currentObject.Empleado.NombreCompleto;
            NOMBRE2.Text = currentObject.Empleado.NombreCompleto;
            CATEGORIA.Text = currentObject.Empleado.StatisticsGroupCode;
            CATEGORIA2.Text = currentObject.Empleado.StatisticsGroupCode;
            FECHA.Text = currentObject.FechaCita.Day + "/" + currentObject.FechaCita.Month + "/" + currentObject.FechaCita.Year;
            FECHA2.Text = currentObject.FechaCita.Day + "/" + currentObject.FechaCita.Month + "/" + currentObject.FechaCita.Year;
            HORA.Text = currentObject.Hora.ToString();
            HORA2.Text = currentObject.Hora.ToString();
            try
            {
                Tipo.Text = currentObject.TipoReconocimiento.Descripcion;
                tipo2.Text = currentObject.TipoReconocimiento.Descripcion;
            }
            catch
            {
                Tipo.Text = "";
                tipo2.Text = "";
            }
            
        }

    }
}
