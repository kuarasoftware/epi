﻿using System;

namespace Epi.Module.Repos
{
    public partial class Ofrecimiento : DevExpress.XtraReports.UI.XtraReport
    {
        public Ofrecimiento(BusinessObjects.Ofrecimientos currentObject)
        {
            InitializeComponent();
            Nombre.Text = currentObject.Empleado.Name;
            try
            {
                Apellidos.Text = currentObject.Empleado.FirstFamilyName + ", " + currentObject.Empleado.SecondFamilyName;
                DNI.Text = currentObject.Empleado.DNI;
                Puesto.Text = currentObject.Empleado.StatisticsGroupCode;
            }
            catch
            {
                Apellidos.Text = "Empleado no identificado";
            }

            Fecha.Text = ".................de.......................de........";// currentObject.Fecha.ToLongDateString();
        }

    }
}
