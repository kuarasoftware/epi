﻿using DevExpress.XtraReports.UI;
using System;
using System.Drawing;
using System.Linq;

namespace Epi.Module.Repos
{
    public partial class DevolucionEpi : DevExpress.XtraReports.UI.XtraReport
    {
        public DevolucionEpi(BusinessObjects.EpisxEmpleado currentObject)
        {
            InitializeComponent();

            Empleado.Text = currentObject.Empleado.NombreCompleto;
            if (currentObject.FechaDevolución != DateTime.MinValue) { FechaDevolucion.Text = currentObject.FechaDevolución.Day + "/" + currentObject.FechaDevolución.Month + "/" + currentObject.FechaDevolución.Year; }
            else
            {
                FechaDevolucion.Text = "-";
            }
            FechaEntrega.Text = currentObject.FechaEntrega.Day + "/" + currentObject.FechaEntrega.Month + "/" + currentObject.FechaEntrega.Year;
            Entregado.Value = currentObject.Entregado;

            var lista = currentObject.EpisxEmpleadoEpis.OrderByDescending(o => o.Estado).ThenBy(o => o.Epis.Descripcion).ToList();

            foreach (var item in lista)
            {
                if (item.Estado == BusinessObjects.EpisxEmpleadoEpis.TextOnlyEnum.Devuelto)
                {
                    XRTableRow fila = this.Epis.Rows[0];

                    if (fila.Cells[0].Text == "TextoPorDefectoNOQuitar")
                    {            // insertamos una nueva row
                        fila = this.Epis.InsertRowBelow(this.xrTableRow1);
                    }

                    // creamos un array con las celdas de la nueva row
                    var celdas = fila.Cells;

                    // llenamos las celdas de la nueva row
                    celdas[0].Text = item.Epis.Descripcion;
                    celdas[1].Text = item.Cantidad.ToString();
                    if (item.FechaDevolucion != DateTime.MinValue) { celdas[2].Text = item.FechaDevolucion.Day + "/" + item.FechaDevolucion.Month + "/" + item.FechaDevolucion.Year; }
                    else
                    {
                        celdas[2].Text = "-";
                    }
                    celdas[3].Text = item.Estado.ToString();
                }
            }
            int valor = 0;

            foreach (XRTableRow item in this.Epis.Rows)
            {
                if ((valor++ % 2) == 0)
                    item.BackColor = Color.LightGray;
            }

            this.Epis.DeleteRow(this.Epis.Rows[0]);
        }

    }
}
