﻿using System;

namespace Epi.Module.Repos
{
    public partial class Cita : DevExpress.XtraReports.UI.XtraReport
    {
        public Cita(BusinessObjects.Citas currentObject)
        {
            InitializeComponent();
            DNI.Text = currentObject.Empleado.DNI;
            NOMBRE.Text = currentObject.Empleado.NombreCompleto;
            CATEGORIA.Text = currentObject.Empleado.StatisticsGroupCode;
            FECHA.Text = currentObject.FechaCita.Day + "/" + currentObject.FechaCita.Month + "/" + currentObject.FechaCita.Year;
            HORA.Text = currentObject.Hora.ToString();
            Tipo.Text = currentObject.TipoReconocimiento.Descripcion;
            Previs.Value = currentObject.Previs;
            Unidad.Value = currentObject.UnidadMovil;
        }

    }
}
