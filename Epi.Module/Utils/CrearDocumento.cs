﻿using DevExpress.Persistent.Base;
using Epi.Module.BusinessObjects;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;


namespace Epi.Module.Utils
{
    [DefaultClassOptions]
    public class CrearDocumento
    {
        public static void CodificarArchivo(DevExpress.ExpressApp.View v, bool b, string s)
        {
            //abrimos el dialogo para escoger el documento

            OpenFileDialog ofd = new OpenFileDialog();
            if (s != string.Empty)
            {
                ofd.Filter = s;
            }
            ofd.ShowDialog();

            try
            {
                FileInfo fi = new FileInfo(ofd.FileName);
                ((Documento)v.CurrentObject).FechaAltaDate = fi.CreationTime;
                ((Documento)v.CurrentObject).FechaModif = fi.LastWriteTime;
                //Se debe escribir como ((Documento)v.CurrentObject)
                ((Documento)v.CurrentObject).RutaFisica = ofd.FileName;
                ((Documento)v.CurrentObject).Extension = Path.GetExtension(ofd.FileName);
                if (b)
                {
                    string sBase64 = string.Empty;
                    // Declaramos fs para tener acceso al archivo residente en la maquina cliente.
                    FileStream fs = new FileStream(ofd.FileName, FileMode.Open);
                    // Declaramos un Leector Binario para accesar a los datos del archivo pasarlos a un arreglo de bytes
                    BinaryReader br = new BinaryReader(fs);
                    byte[] bytes = new byte[(int)fs.Length];

                    try
                    {
                        br.Read(bytes, 0, bytes.Length);
                        ((Documento)v.CurrentObject).IDOCUMENTO = bytes;
                    }
                    catch
                    {
                        ((Documento)v.CurrentObject).IDOCUMENTO = null;
                    }
                }
            }
            catch
            {
                MessageBox.Show("No has seleccionado ningún archivo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public static void AbrirFichero(Documento d)
        {

            string exten = d.Extension;

            try
            {
                byte[] aDocument = d.IDOCUMENTO;
                byte[] MyData = new byte[aDocument.Length - 2];
                int ArraySize = new int();
                ArraySize = MyData.GetUpperBound(0);
                Guid g;
                g = Guid.NewGuid();
                if (exten == ".pdf")
                {
                    exten = string.Empty;
                }
                string temp = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\" + g.ToString() + exten;
                //try
                //{
                //    File.Delete(temp);
                //}
                //catch (Exception e)
                //{
                //}
                FileStream fs = new FileStream(temp, FileMode.CreateNew, FileAccess.Write, FileShare.Write, aDocument.Length - 2);
                //fs.Write(MyData, 0, ArraySize);
                for (int i = 0; i <= MyData.Length - 1; i++)
                {
                    MyData[i] = aDocument[i];
                }
                fs.Write(MyData, 0, MyData.Length);
                fs.Close();
                System.Diagnostics.Process P = new System.Diagnostics.Process();
                try
                {
                    P.StartInfo.FileName = temp;
                    P.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                    P.Start();
                    P.WaitForExit();
                    P.Close();
                }
                catch { }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se puede abrir el fichero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ////ID
            //intI++;
            //objEmpleados.Identificador = dtDataTable.Rows[intPosicionEnDataTable][intI].ToString();
        }
    }
}