﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace Epi.Module.Utils
{
    public class EnvioSMS
    {
        // Envío de SMS
        // Cliente web   
        public EnvioSMS()
        {
        }
        public string enviaSMS(ref BusinessObjects.Ofrecimientos Ofre, string destinatario, string mensaje)
        {
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                // Ahora se añaden los parámetros de la petición
                client.QueryString.Add("Correo", ConfigurationManager.AppSettings["CorreoMensatek"]);
                client.QueryString.Add("Passwd", ConfigurationManager.AppSettings["PassweordMensatek"]);
                client.QueryString.Add("Remitente", ConfigurationManager.AppSettings["RemitenteMensatek"]);
                client.QueryString.Add("Destinatarios", "34" + destinatario);
                client.QueryString.Add("Mensaje", mensaje);
                client.QueryString.Add("Descuento", "0");
                client.QueryString.Add("Resp", "JSON");

                string baseurl = "http://api.mensatek.com/sms/v5/enviar.php";
                // Se abre la petición y se lee la respuesta
                Stream data = client.OpenRead(baseurl);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
                Ofre.ComumicadoSMS = true;
                Ofre.Save();
                // Devolvemos el string para ver el resultado
                return (s);
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se puede enviar el mensaje " + ex.ToString());
                return (string.Empty);
            }
        }
    }
}
