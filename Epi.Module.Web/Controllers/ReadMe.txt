﻿Folder Description

The "Controllers" project folder is intended for storing ASP.NET-specific Controller classes 
that can change the default XAF application flow and add new features.


Relevant Documentation

Extend Functionality
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112623.

Controller Class
https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppControllertopic.

ViewController Class
https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.

WindowController Class
https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppWindowControllertopic.
