﻿Folder Description

This project folder is intended for storing custom ASP.NET List Editors, 
Property Editors and View Items.


Relevant Documentation

List Editors
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113189.

Implement Custom Property Editors
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113097.

Implement Custom View Items
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112695.

How to: Implement an ASP.NET Web List Editor Using a Custom Control
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113126.

How to: Implement a Property Editor (in ASP.NET Applications)
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112678.

How to: Implement a Property Editor for Specific Data Management (in ASP.NET Web Applications)
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113116.

How to: Customize a Built-in Web Property Editor
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113114.

How to: Implement a View Item
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112641.
