﻿
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.Persistent.Base;
using DevExpress.Skins;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace Epi.Win
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if EASYTEST
            DevExpress.ExpressApp.Win.EasyTest.EasyTestRemotingRegistration.Register();
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            EditModelPermission.AlwaysGranted = System.Diagnostics.Debugger.IsAttached;
            if (Tracing.GetFileLocationFromSettings() == DevExpress.Persistent.Base.FileLocation.CurrentUserApplicationDataFolder)
            {
                Tracing.LocalUserAppDataPath = Application.LocalUserAppDataPath;
            }
            Tracing.Initialize();
            EpiWindowsFormsApplication winApplication = new EpiWindowsFormsApplication();
            // Refer to the https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112680. help article for more details on how to provide a custom splash form.
            //winApplication.SplashScreen = new DevExpress.ExpressApp.Win.Utils.DXSplashScreen("YourSplashImage.png");
            SecurityAdapterHelper.Enable();
            if (ConfigurationManager.ConnectionStrings["ConnectionString"] != null)
            {
                winApplication.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
#if EASYTEST
            if(ConfigurationManager.ConnectionStrings["EasyTestConnectionString"] != null) {
                winApplication.ConnectionString = ConfigurationManager.ConnectionStrings["EasyTestConnectionString"].ConnectionString;
            }
#endif
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached && winApplication.CheckCompatibilityType == CheckCompatibilityType.DatabaseSchema)
            {
                winApplication.DatabaseUpdateMode = DatabaseUpdateMode.UpdateDatabaseAlways;
            }
#endif
            
                try
            {
                winApplication.Setup();
                CreaSMS();
                winApplication.Start();
            }
            catch (Exception e)
            {
                winApplication.HandleException(e);
            }
        }

        private static void CreaSMS()
        {
            try
            {
                DevExpress.Xpo.Session session = new DevExpress.Xpo.Session();
                DevExpress.Xpo.XPCollection<Epi.Module.BusinessObjects.Ofrecimientos> ListaOfrecimientos = new DevExpress.Xpo.XPCollection<Epi.Module.BusinessObjects.Ofrecimientos>(session, DevExpress.Data.Filtering.CriteriaOperator.Parse("[ComumicadoSMS]=?", false));
                foreach (Epi.Module.BusinessObjects.Ofrecimientos Ofres in ListaOfrecimientos)
                {
                    Epi.Module.Utils.EnvioSMS envioSMS = new Module.Utils.EnvioSMS();
                    Epi.Module.BusinessObjects.Ofrecimientos Ofre = Ofres;
                    envioSMS.enviaSMS(ref Ofre, Ofre.Empleado.MobilePhoneNo, "Tinen una revisión médica Dia" + Ofre.Fecha.ToShortDateString());
                }
            }
            catch (Exception e)
            { }
        }
    }
}
