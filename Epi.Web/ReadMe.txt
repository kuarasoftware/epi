﻿Project Description

This project implements an ASP.NET application. The ApplicationCode project 
folder contains the WebApplication.cs(vb) file with the class that inherits 
WebApplication. This class can be designed with the Application Designer that 
allows you to view and customize application components: referenced modules, 
security settings, data connection. Additionally, the root folder contains 
Application Model difference files (XAFML files) that keep application settings 
specific for the current application. Differences files can be designed with 
the Model Editor.  


Relevant Documentation

Application Solution Components
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112569.

WebApplication Class
https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppWebWebApplicationtopic.

XafApplication Class
https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppXafApplicationtopic.

Application Designer
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112827.

Application Model
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112579.

Model Editor
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112582.


eXpand Framework - http://www.expandframework.com

This is an open source toolkit built above the eXpressApp Framework extending its capabilities
and providing 50+ cutting-edge libraries containing tools and reusable modules to target numerous 
business scenarios. The main idea behind eXpand is to offer as many features as possible to 
developers/business users through a declarative approach (configuring files rather than writing code).
