﻿
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.UI;
using System;
using System.Drawing;
using System.Linq;

namespace Epi.Module.Win.Controllers
{

    public class EventCustomSchedulerViewController : ObjectViewController<ObjectView, Event>
    {
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();

            if (View is ListView)
            {
                EditInListView();
                ListView view = (ListView)View;
            }
            else if (View is DetailView)
            {
                EditInDetailView();
            }
        }

        protected override void BeginUpdate()
        {
            base.BeginUpdate();
        }

        private void DoSomething(object sender, AppointmentOperationEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void EditInDetailView()
        {
            EditLabelsDetailView();
            EditStatussDetailView();
        }

        private void EditStatussDetailView()
        {
            foreach (SchedulerStatusPropertyEditor status in ((DetailView)View).GetItems<SchedulerStatusPropertyEditor>())
            {
                if (status.Control != null)
                {
                    SetupStatuses(((AppointmentStatusEdit)status.Control).Storage);
                }
                else
                {
                    status.ControlCreated += status_ControlCreated;
                }
            }
        }

        private void EditLabelsDetailView()
        {

            foreach (SchedulerLabelPropertyEditor label in ((DetailView)View).GetItems<SchedulerLabelPropertyEditor>())
            {
                if (label.Control != null)
                {
                    SetupLabels(((AppointmentLabelEdit)label.Control).Storage);
                }
                else
                {
                    label.ControlCreated += label_ControlCreated;
                }
            }
        }

        private void EditInListView()
        {
            SchedulerListEditor listEditor = ((ListView)View).Editor as SchedulerListEditor;
            //listEditor.AllowEdit = true;
            //listEditor.OptionsCustomization.AllowAppointmentEdit = UsedAppointmentType.All;
            if (listEditor != null)
            {
                SetupLabels(listEditor.SchedulerControl.Storage);
                SetupStatuses(listEditor.SchedulerControl.Storage);
            }
        }

        void label_ControlCreated(object sender, EventArgs e)
        {
            var control = (AppointmentLabelEdit)((SchedulerLabelPropertyEditor)sender).Control;
            SetupLabels(control.Storage);
            control.RefreshData();
        }
        void status_ControlCreated(object sender, EventArgs e)
        {
            var control = (AppointmentStatusEdit)((SchedulerStatusPropertyEditor)sender).Control;
            SetupStatuses(control.Storage);
            control.RefreshData();
        }
        private void SetupLabels(ISchedulerStorageBase storage)
        {
            storage.Appointments.Labels.Clear();

            AddLabel(storage, Color.Khaki, "Ofrecimiento");
            AddLabel(storage, Color.MediumAquamarine, "Citas");
            AddLabel(storage, Color.Coral, "Reconocimiento");

        }
        private static void AddLabel(ISchedulerStorageBase storage, Color color, string caption)
        {
            var item = storage.Appointments.Labels.CreateNewLabel(caption);
            item.SetColor(color);
            storage.Appointments.Labels.Add(item);
        }
        private void SetupStatuses(ISchedulerStorageBase storage)
        {
            //  storage.Appointments.Statuses.Clear();

            //    storage.Appointments.Statuses.Add(label.Value, label.Key)

        }
    }
}