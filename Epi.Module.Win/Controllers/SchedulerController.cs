﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.Persistent.Base.General;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Drawing;
using System;
using System.Linq;

namespace Epi.Module.Win.Controllers
{
    public partial class SchedulerController : ViewController
    {
        public SchedulerController()
        {
            InitializeComponent();
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            if (View.ObjectTypeInfo.Implements<IEvent>())
                View.ControlsCreated += new EventHandler(View_ControlsCreated);
        }

        void View_ControlsCreated(object sender, EventArgs e)
        {
            try
            {
                ListView view = (ListView)View;
                SchedulerListEditor listEditor = (SchedulerListEditor)view.Editor;
                SchedulerControl scheduler = listEditor.SchedulerControl;
                scheduler.ActiveViewType = SchedulerViewType.Month;
                scheduler.WeekView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;
                scheduler.MonthView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;
                scheduler.DayView.AppointmentDisplayOptions.EndTimeVisibility = AppointmentTimeVisibility.Never;
                scheduler.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                scheduler.MonthView.SelectedInterval.AllDay = true;
                

            }
            catch { }
        }

       
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            if (View.ObjectTypeInfo.Implements<IEvent>())
            {
                View.ControlsCreated -= new EventHandler(View_ControlsCreated);
            }
        }
    }
}
