﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using Epi.Module.BusinessObjects;
using Epi.Module.Win.forms;
using System.Drawing;

namespace Epi.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Botones : ViewController
    {
        public Botones()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        PantallaFirma pantallaFirma;
        static EpisxEmpleado EpisxEmpleado;
        private void Firmar_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            pantallaFirma = new PantallaFirma();
            pantallaFirma.Show();
            pantallaFirma.FormClosed += PantallaFirma_FormClosed;
            if (((EpisxEmpleado)View.CurrentObject) != null)
            {
                EpisxEmpleado = ((EpisxEmpleado)View.CurrentObject);
            }
        }

        private void PantallaFirma_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            if(pantallaFirma.sigimage != null)
            {
                EpisxEmpleado.Firma = pantallaFirma.sigimage;
                EpisxEmpleado.Save();
                EpisxEmpleado.Session.CommitTransaction();
            }
        }
    }
}
