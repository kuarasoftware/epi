﻿namespace Epi.Module.Win.Controllers
{
    partial class Botones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Firmar = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // Firmar
            // 
            this.Firmar.Caption = "Firmar";
            this.Firmar.ConfirmationMessage = null;
            this.Firmar.Id = "Firmar";
            this.Firmar.TargetObjectType = typeof(Epi.Module.BusinessObjects.EpisxEmpleado);
            this.Firmar.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.Firmar.ToolTip = null;
            this.Firmar.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.Firmar.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Firmar_Execute);
            // 
            // Botones
            // 
            this.Actions.Add(this.Firmar);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction Firmar;
    }
}
