﻿Folder Description

This project folder is intended for storing custom WinForms List Editors, 
Property Editors and View Items.

Relevant Documentation

List Editors
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113189.

Implement Custom Property Editors
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113097.

Implement Custom View Items
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112695.

How to: Implement a Custom WinForms List Editor
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112659.

How to: Support a Context Menu for a Custom WinForms List Editor
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112660.

How to: Implement a Property Editor (in WinForms Applications)
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112679.

How to: Implement a Property Editor for Specific Data Management (in WinForms Applications)
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113101.

How to: Extend Built-in Property Editor's Functionality
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113104.

How to: Implement a View Item
https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112641.
