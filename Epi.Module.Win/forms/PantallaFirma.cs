﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace Epi.Module.Win.forms
{
    public partial class PantallaFirma : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public System.Drawing.Image sigimage;

        public PantallaFirma()
        {
            InitializeComponent();
            sigPlusNET1.SetTabletState(1);
        }

        private void Borrar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sigPlusNET1.ClearTablet();
        }

        private void Guardar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (sigPlusNET1.NumberOfTabletPoints() == 0)
            {
                //User must sign first!!
            }
            else
            {
                sigPlusNET1.SetTabletState(0); //turn off signature pad

                try
                {


                    sigPlusNET1.SetImageXSize(1000);
                    sigPlusNET1.SetImageYSize(300);
                    sigPlusNET1.SetJustifyY(10);
                    sigPlusNET1.SetJustifyX(10);
                    sigPlusNET1.SetJustifyMode(5);
                    sigPlusNET1.SetImagePenWidth(10);
                    sigPlusNET1.SetImageFileFormat(4); //0=bmp, 4=jpg, 6=tif
                    sigimage = sigPlusNET1.GetSigImage();

                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(sigimage, System.Drawing.Imaging.ImageFormat.Jpeg);
                    image.ScalePercent(20);
                    image.Transparency = new int[] { 255, 255 };
                    image.SetAbsolutePosition(100, 200);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            this.Close();
        }
    }
}
